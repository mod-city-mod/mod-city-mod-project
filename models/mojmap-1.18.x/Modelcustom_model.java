// Made with Blockbench 4.4.3
// Exported for Minecraft version 1.17 - 1.18 with Mojang mappings
// Paste this class into your mod and generate all required imports

public class Modelcustom_model<T extends Entity> extends EntityModel<T> {
	// This layer location should be baked with EntityRendererProvider.Context in
	// the entity renderer and passed into this model's constructor
	public static final ModelLayerLocation LAYER_LOCATION = new ModelLayerLocation(
			new ResourceLocation("modid", "custom_model"), "main");
	private final ModelPart frontWheels;
	private final ModelPart backWheels;
	private final ModelPart kart;

	public Modelcustom_model(ModelPart root) {
		this.frontWheels = root.getChild("frontWheels");
		this.backWheels = root.getChild("backWheels");
		this.kart = root.getChild("kart");
	}

	public static LayerDefinition createBodyLayer() {
		MeshDefinition meshdefinition = new MeshDefinition();
		PartDefinition partdefinition = meshdefinition.getRoot();

		PartDefinition frontWheels = partdefinition.addOrReplaceChild("frontWheels", CubeListBuilder.create(),
				PartPose.offset(0.0F, 22.0F, -8.5F));

		PartDefinition cube_r1 = frontWheels
				.addOrReplaceChild("cube_r1",
						CubeListBuilder.create().texOffs(22, 11).addBox(-9.0F, -1.5F, -7.0F, 1.0F, 1.0F, 14.0F,
								new CubeDeformation(0.0F)),
						PartPose.offsetAndRotation(0.0F, 1.0F, 8.5F, 0.0F, -1.5708F, 0.0F));

		PartDefinition wheel = frontWheels.addOrReplaceChild("wheel", CubeListBuilder.create(),
				PartPose.offset(-9.0F, 0.0F, 16.5F));

		PartDefinition cube_r2 = wheel.addOrReplaceChild("cube_r2",
				CubeListBuilder.create().texOffs(42, 39).addBox(-10.5F, -3.0F, 7.0F, 4.0F, 4.0F, 2.0F,
						new CubeDeformation(0.0F)),
				PartPose.offsetAndRotation(9.0F, 1.0F, -8.0F, 0.0F, -1.5708F, 0.0F));

		PartDefinition wheel3 = frontWheels.addOrReplaceChild("wheel3", CubeListBuilder.create(),
				PartPose.offset(-9.0F, 0.0F, 0.5F));

		PartDefinition cube_r3 = wheel3
				.addOrReplaceChild("cube_r3",
						CubeListBuilder.create().texOffs(18, 45).addBox(-10.5F, -3.0F, -9.0F, 4.0F, 4.0F, 2.0F,
								new CubeDeformation(0.0F)),
						PartPose.offsetAndRotation(9.0F, 1.0F, 8.0F, 0.0F, -1.5708F, 0.0F));

		PartDefinition backWheels = partdefinition.addOrReplaceChild("backWheels", CubeListBuilder.create(),
				PartPose.offset(0.0F, 22.0F, 8.5F));

		PartDefinition cube_r4 = backWheels.addOrReplaceChild("cube_r4",
				CubeListBuilder.create().texOffs(0, 22).addBox(8.0F, -1.5F, -7.0F, 1.0F, 1.0F, 14.0F,
						new CubeDeformation(0.0F)),
				PartPose.offsetAndRotation(0.0F, 1.0F, -8.5F, 0.0F, -1.5708F, 0.0F));

		PartDefinition wheel4 = backWheels.addOrReplaceChild("wheel4", CubeListBuilder.create(),
				PartPose.offset(9.0F, 0.0F, -16.5F));

		PartDefinition cube_r5 = wheel4.addOrReplaceChild("cube_r5",
				CubeListBuilder.create().texOffs(10, 37).addBox(6.5F, -3.0F, -9.0F, 4.0F, 4.0F, 2.0F,
						new CubeDeformation(0.0F)),
				PartPose.offsetAndRotation(-9.0F, 1.0F, 8.0F, 0.0F, -1.5708F, 0.0F));

		PartDefinition wheel2 = backWheels.addOrReplaceChild("wheel2", CubeListBuilder.create(),
				PartPose.offset(9.0F, 0.0F, -0.5F));

		PartDefinition cube_r6 = wheel2.addOrReplaceChild("cube_r6",
				CubeListBuilder.create().texOffs(38, 26).addBox(6.5F, -3.0F, 7.0F, 4.0F, 4.0F, 2.0F,
						new CubeDeformation(0.0F)),
				PartPose.offsetAndRotation(-9.0F, 1.0F, -8.0F, 0.0F, -1.5708F, 0.0F));

		PartDefinition kart = partdefinition.addOrReplaceChild("kart", CubeListBuilder.create(),
				PartPose.offset(0.0F, 23.0F, 0.0F));

		PartDefinition cube_r7 = kart.addOrReplaceChild("cube_r7",
				CubeListBuilder.create().texOffs(0, 19)
						.addBox(-6.0F, -8.0F, -1.5F, 0.0F, 3.0F, 3.0F, new CubeDeformation(0.0F)).texOffs(20, 22)
						.addBox(-7.0F, -7.0F, -0.5F, 1.0F, 1.0F, 1.0F, new CubeDeformation(0.0F)),
				PartPose.offsetAndRotation(0.0F, 0.0F, 0.0F, 1.5708F, -1.3526F, -1.5708F));

		PartDefinition cube_r8 = kart.addOrReplaceChild("cube_r8",
				CubeListBuilder.create().texOffs(0, 37)
						.addBox(-9.0F, -6.0F, -4.0F, 1.0F, 3.0F, 8.0F, new CubeDeformation(0.0F)).texOffs(34, 45)
						.addBox(-10.0F, -5.0F, 5.0F, 3.0F, 2.0F, 2.0F, new CubeDeformation(0.0F)).texOffs(34, 35)
						.addBox(-5.0F, -3.0F, 5.0F, 10.0F, 2.0F, 2.0F, new CubeDeformation(0.0F)).texOffs(34, 35)
						.addBox(-5.0F, -3.0F, -7.0F, 10.0F, 2.0F, 2.0F, new CubeDeformation(0.0F)).texOffs(22, 35)
						.addBox(-16.0F, -3.0F, -4.0F, 2.0F, 2.0F, 8.0F, new CubeDeformation(0.0F)).texOffs(32, 45)
						.addBox(-10.0F, -5.0F, -7.0F, 3.0F, 2.0F, 2.0F, new CubeDeformation(0.0F)).texOffs(16, 26)
						.addBox(-5.0F, -4.0F, -4.0F, 7.0F, 1.0F, 8.0F, new CubeDeformation(0.0F)).texOffs(0, 0)
						.addBox(-13.0F, -3.0F, -5.0F, 25.0F, 1.0F, 10.0F, new CubeDeformation(0.0F)),
				PartPose.offsetAndRotation(0.0F, 0.0F, 0.0F, 0.0F, -1.5708F, 0.0F));

		PartDefinition cube_r9 = kart.addOrReplaceChild("cube_r9",
				CubeListBuilder.create().texOffs(32, 45)
						.addBox(-9.3F, -9.825F, 5.0F, 4.0F, 2.0F, 2.0F, new CubeDeformation(0.0F)).texOffs(31, 45)
						.addBox(-9.3F, -9.825F, -7.0F, 4.0F, 2.0F, 2.0F, new CubeDeformation(0.0F)),
				PartPose.offsetAndRotation(0.0F, 0.0F, 0.0F, 1.5708F, -0.9599F, -1.5708F));

		PartDefinition cube_r10 = kart.addOrReplaceChild("cube_r10",
				CubeListBuilder.create().texOffs(32, 45)
						.addBox(-8.6F, -0.075F, 5.0F, 4.0F, 2.0F, 2.0F, new CubeDeformation(0.0F)).texOffs(32, 45)
						.addBox(-8.6F, -0.075F, -7.0F, 4.0F, 2.0F, 2.0F, new CubeDeformation(0.0F)),
				PartPose.offsetAndRotation(0.0F, 0.0F, 0.0F, -1.5708F, -0.9599F, 1.5708F));

		PartDefinition cube_r11 = kart.addOrReplaceChild("cube_r11",
				CubeListBuilder.create().texOffs(32, 45)
						.addBox(-9.3F, -9.825F, 5.0F, 4.0F, 2.0F, 2.0F, new CubeDeformation(0.0F)).texOffs(31, 45)
						.addBox(-9.3F, -9.825F, -7.0F, 4.0F, 2.0F, 2.0F, new CubeDeformation(0.0F)),
				PartPose.offsetAndRotation(0.0F, 0.0F, 17.0F, 1.5708F, -0.9599F, -1.5708F));

		PartDefinition cube_r12 = kart.addOrReplaceChild("cube_r12",
				CubeListBuilder.create().texOffs(34, 45)
						.addBox(-10.0F, -5.0F, 5.0F, 3.0F, 2.0F, 2.0F, new CubeDeformation(0.0F)).texOffs(32, 45)
						.addBox(-10.0F, -5.0F, -7.0F, 3.0F, 2.0F, 2.0F, new CubeDeformation(0.0F)),
				PartPose.offsetAndRotation(0.0F, 0.0F, 17.0F, 0.0F, -1.5708F, 0.0F));

		PartDefinition cube_r13 = kart.addOrReplaceChild("cube_r13",
				CubeListBuilder.create().texOffs(32, 45)
						.addBox(-8.6F, -0.075F, 5.0F, 4.0F, 2.0F, 2.0F, new CubeDeformation(0.0F)).texOffs(32, 45)
						.addBox(-8.6F, -0.075F, -7.0F, 4.0F, 2.0F, 2.0F, new CubeDeformation(0.0F)),
				PartPose.offsetAndRotation(0.0F, 0.0F, 17.0F, -1.5708F, -0.9599F, 1.5708F));

		PartDefinition cube_r14 = kart.addOrReplaceChild("cube_r14",
				CubeListBuilder.create().texOffs(30, 45).addBox(-9.75F, -3.0F, 11.25F, 5.0F, 2.0F, 2.0F,
						new CubeDeformation(0.0F)),
				PartPose.offsetAndRotation(0.0F, 0.0F, 0.0F, -3.1416F, -0.8727F, 3.1416F));

		PartDefinition cube_r15 = kart.addOrReplaceChild("cube_r15",
				CubeListBuilder.create().texOffs(16, 22)
						.addBox(10.0F, 3.0F, 1.0F, 1.0F, 3.0F, 2.0F, new CubeDeformation(0.0F)).texOffs(16, 22)
						.addBox(10.0F, 3.0F, -3.0F, 1.0F, 3.0F, 2.0F, new CubeDeformation(0.0F)),
				PartPose.offsetAndRotation(0.0F, 0.0F, 0.0F, 1.5708F, -0.829F, -1.5708F));

		PartDefinition cube_r16 = kart
				.addOrReplaceChild("cube_r16",
						CubeListBuilder.create().texOffs(30, 45).addBox(-9.75F, -3.0F, -13.25F, 5.0F, 2.0F, 2.0F,
								new CubeDeformation(0.0F)),
						PartPose.offsetAndRotation(0.0F, 0.0F, 0.0F, 0.0F, -0.8727F, 0.0F));

		PartDefinition cube_r17 = kart.addOrReplaceChild("cube_r17",
				CubeListBuilder.create().texOffs(0, 22).addBox(0.0F, -11.0F, -3.0F, 1.0F, 7.0F, 6.0F,
						new CubeDeformation(0.0F)),
				PartPose.offsetAndRotation(0.0F, 0.0F, 0.0F, -1.5708F, -1.309F, 1.5708F));

		PartDefinition cube_r18 = kart.addOrReplaceChild("cube_r18",
				CubeListBuilder.create().texOffs(0, 11).addBox(-9.0F, -11.0F, -5.0F, 8.0F, 1.0F, 10.0F,
						new CubeDeformation(0.0F)),
				PartPose.offsetAndRotation(0.0F, 0.0F, 0.0F, 1.5708F, -0.8727F, -1.5708F));

		PartDefinition engine = kart.addOrReplaceChild("engine", CubeListBuilder.create(),
				PartPose.offset(0.0F, 0.0F, 0.0F));

		PartDefinition cube_r19 = engine.addOrReplaceChild("cube_r19",
				CubeListBuilder.create().texOffs(38, 15)
						.addBox(5.0F, -6.0F, -2.0F, 5.0F, 3.0F, 4.0F, new CubeDeformation(0.0F)).texOffs(0, 11)
						.addBox(7.0F, -7.0F, 2.0F, 3.0F, 3.0F, 2.0F, new CubeDeformation(0.0F)),
				PartPose.offsetAndRotation(0.0F, 0.0F, 0.0F, 0.0F, -1.5708F, 0.0F));

		PartDefinition piece1 = engine.addOrReplaceChild("piece1", CubeListBuilder.create(),
				PartPose.offset(0.0F, 0.0F, 0.0F));

		PartDefinition cube_r20 = piece1
				.addOrReplaceChild("cube_r20",
						CubeListBuilder.create().texOffs(0, 5).addBox(6.0F, -7.0F, -3.0F, 3.0F, 3.0F, 2.0F,
								new CubeDeformation(0.0F)),
						PartPose.offsetAndRotation(0.0F, 0.0F, 0.0F, 0.0F, -1.5708F, 0.0F));

		PartDefinition pipe = engine.addOrReplaceChild("pipe", CubeListBuilder.create(),
				PartPose.offset(4.0F, -5.0F, 9.0F));

		PartDefinition cube_r21 = pipe.addOrReplaceChild("cube_r21",
				CubeListBuilder.create().texOffs(0, 0).addBox(8.0F, -6.0F, -5.0F, 2.0F, 2.0F, 3.0F,
						new CubeDeformation(0.0F)),
				PartPose.offsetAndRotation(-4.0F, 5.0F, -9.0F, 0.0F, -1.5708F, 0.0F));

		PartDefinition cube_r22 = pipe.addOrReplaceChild("cube_r22",
				CubeListBuilder.create().texOffs(44, 45).addBox(11.0F, -2.25F, -5.0F, 5.0F, 2.0F, 2.0F,
						new CubeDeformation(0.0F)),
				PartPose.offsetAndRotation(-4.0F, 5.0F, -9.0F, 1.5708F, -1.2217F, -1.5708F));

		return LayerDefinition.create(meshdefinition, 128, 128);
	}

	@Override
	public void renderToBuffer(PoseStack poseStack, VertexConsumer vertexConsumer, int packedLight, int packedOverlay,
			float red, float green, float blue, float alpha) {
		frontWheels.render(poseStack, vertexConsumer, packedLight, packedOverlay, red, green, blue, alpha);
		backWheels.render(poseStack, vertexConsumer, packedLight, packedOverlay, red, green, blue, alpha);
		kart.render(poseStack, vertexConsumer, packedLight, packedOverlay, red, green, blue, alpha);
	}

	public void setupAnim(T entity, float limbSwing, float limbSwingAmount, float ageInTicks, float netHeadYaw,
			float headPitch) {
		this.frontWheels.xRot = Mth.cos(limbSwing * 1.0F) * -1.0F * limbSwingAmount;
		this.backWheels.xRot = Mth.cos(limbSwing * 1.0F) * -1.0F * limbSwingAmount;
	}
}