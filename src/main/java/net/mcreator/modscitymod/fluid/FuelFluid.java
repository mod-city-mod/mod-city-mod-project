
package net.mcreator.modscitymod.fluid;

import net.minecraftforge.fluids.ForgeFlowingFluid;
import net.minecraftforge.fluids.FluidAttributes;

import net.minecraft.world.level.material.FluidState;
import net.minecraft.world.level.material.Fluid;
import net.minecraft.world.level.block.state.StateDefinition;
import net.minecraft.world.level.block.LiquidBlock;
import net.minecraft.resources.ResourceLocation;

import net.mcreator.modscitymod.init.ModsCityModModItems;
import net.mcreator.modscitymod.init.ModsCityModModFluids;
import net.mcreator.modscitymod.init.ModsCityModModBlocks;

public abstract class FuelFluid extends ForgeFlowingFluid {
	public static final ForgeFlowingFluid.Properties PROPERTIES = new ForgeFlowingFluid.Properties(ModsCityModModFluids.FUEL, ModsCityModModFluids.FLOWING_FUEL,
			FluidAttributes.builder(new ResourceLocation("mods_city_mod:blocks/fuelstill"), new ResourceLocation("mods_city_mod:blocks/fuelflow"))

	).explosionResistance(100f)

			.bucket(ModsCityModModItems.FUEL_BUCKET).block(() -> (LiquidBlock) ModsCityModModBlocks.FUEL.get());

	private FuelFluid() {
		super(PROPERTIES);
	}

	public static class Source extends FuelFluid {
		public Source() {
			super();
		}

		public int getAmount(FluidState state) {
			return 8;
		}

		public boolean isSource(FluidState state) {
			return true;
		}
	}

	public static class Flowing extends FuelFluid {
		public Flowing() {
			super();
		}

		protected void createFluidStateDefinition(StateDefinition.Builder<Fluid, FluidState> builder) {
			super.createFluidStateDefinition(builder);
			builder.add(LEVEL);
		}

		public int getAmount(FluidState state) {
			return state.getValue(LEVEL);
		}

		public boolean isSource(FluidState state) {
			return false;
		}
	}
}
