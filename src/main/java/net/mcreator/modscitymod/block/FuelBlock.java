
package net.mcreator.modscitymod.block;

import net.minecraft.world.level.material.MaterialColor;
import net.minecraft.world.level.material.Material;
import net.minecraft.world.level.material.FlowingFluid;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.level.block.state.BlockBehaviour;
import net.minecraft.world.level.block.LiquidBlock;
import net.minecraft.world.level.BlockGetter;
import net.minecraft.core.BlockPos;

import net.mcreator.modscitymod.init.ModsCityModModFluids;

public class FuelBlock extends LiquidBlock {
	public FuelBlock() {
		super(() -> (FlowingFluid) ModsCityModModFluids.FUEL.get(), BlockBehaviour.Properties.of(Material.WATER, MaterialColor.COLOR_GREEN).strength(100f)

		);
	}

	@Override
	public boolean propagatesSkylightDown(BlockState state, BlockGetter reader, BlockPos pos) {
		return true;
	}
}
