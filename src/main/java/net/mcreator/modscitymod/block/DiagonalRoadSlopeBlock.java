
package net.mcreator.modscitymod.block;

import net.minecraftforge.api.distmarker.OnlyIn;
import net.minecraftforge.api.distmarker.Dist;

import net.minecraft.world.phys.shapes.VoxelShape;
import net.minecraft.world.phys.shapes.Shapes;
import net.minecraft.world.phys.shapes.CollisionContext;
import net.minecraft.world.phys.HitResult;
import net.minecraft.world.level.storage.loot.LootContext;
import net.minecraft.world.level.material.Material;
import net.minecraft.world.level.material.Fluids;
import net.minecraft.world.level.material.FluidState;
import net.minecraft.world.level.block.state.properties.DirectionProperty;
import net.minecraft.world.level.block.state.properties.BooleanProperty;
import net.minecraft.world.level.block.state.properties.BlockStateProperties;
import net.minecraft.world.level.block.state.StateDefinition;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.level.block.state.BlockBehaviour;
import net.minecraft.world.level.block.SoundType;
import net.minecraft.world.level.block.SimpleWaterloggedBlock;
import net.minecraft.world.level.block.Rotation;
import net.minecraft.world.level.block.Mirror;
import net.minecraft.world.level.block.HorizontalDirectionalBlock;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.LevelAccessor;
import net.minecraft.world.level.BlockGetter;
import net.minecraft.world.item.context.BlockPlaceContext;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.entity.player.Player;
import net.minecraft.core.Direction;
import net.minecraft.core.BlockPos;
import net.minecraft.client.renderer.RenderType;
import net.minecraft.client.renderer.ItemBlockRenderTypes;

import net.mcreator.modscitymod.init.ModsCityModModBlocks;

import java.util.List;
import java.util.Collections;

public class DiagonalRoadSlopeBlock extends Block implements SimpleWaterloggedBlock {
	public static final DirectionProperty FACING = HorizontalDirectionalBlock.FACING;
	public static final BooleanProperty WATERLOGGED = BlockStateProperties.WATERLOGGED;

	public DiagonalRoadSlopeBlock() {
		super(BlockBehaviour.Properties.of(Material.STONE).sound(SoundType.STONE).strength(1f, 10f).noOcclusion().isRedstoneConductor((bs, br, bp) -> false));
		this.registerDefaultState(this.stateDefinition.any().setValue(FACING, Direction.NORTH).setValue(WATERLOGGED, false));
	}

	@Override
	public boolean skipRendering(BlockState state, BlockState adjacentBlockState, Direction side) {
		return adjacentBlockState.getBlock() == this ? true : super.skipRendering(state, adjacentBlockState, side);
	}

	@Override
	public boolean propagatesSkylightDown(BlockState state, BlockGetter reader, BlockPos pos) {
		return state.getFluidState().isEmpty();
	}

	@Override
	public int getLightBlock(BlockState state, BlockGetter worldIn, BlockPos pos) {
		return 0;
	}

	@Override
	public VoxelShape getVisualShape(BlockState state, BlockGetter world, BlockPos pos, CollisionContext context) {
		return Shapes.empty();
	}

	@Override
	public VoxelShape getShape(BlockState state, BlockGetter world, BlockPos pos, CollisionContext context) {
		return switch (state.getValue(FACING)) {
			default -> Shapes.or(box(15, 0, 15, 16, 16, 16), box(14, 0, 14, 16, 16, 15), box(13, 0, 13, 16, 16, 14), box(12, 0, 12, 16, 16, 13), box(11, 0, 11, 16, 16, 12), box(10, 0, 10, 16, 16, 11), box(9, 0, 9, 16, 16, 10),
					box(8, 0, 8, 16, 16, 9), box(7, 0, 7, 16, 16, 8), box(6, 0, 6, 16, 16, 7), box(5, 0, 5, 16, 16, 6), box(4, 0, 4, 16, 16, 5), box(3, 0, 3, 16, 16, 4), box(2, 0, 2, 16, 16, 3), box(1, 0, 1, 16, 16, 2), box(0, 0, 0, 16, 16, 1));
			case NORTH -> Shapes.or(box(0, 0, 0, 1, 16, 1), box(0, 0, 1, 2, 16, 2), box(0, 0, 2, 3, 16, 3), box(0, 0, 3, 4, 16, 4), box(0, 0, 4, 5, 16, 5), box(0, 0, 5, 6, 16, 6), box(0, 0, 6, 7, 16, 7), box(0, 0, 7, 8, 16, 8),
					box(0, 0, 8, 9, 16, 9), box(0, 0, 9, 10, 16, 10), box(0, 0, 10, 11, 16, 11), box(0, 0, 11, 12, 16, 12), box(0, 0, 12, 13, 16, 13), box(0, 0, 13, 14, 16, 14), box(0, 0, 14, 15, 16, 15), box(0, 0, 15, 16, 16, 16));
			case EAST -> Shapes.or(box(15, 0, 0, 16, 16, 1), box(14, 0, 0, 15, 16, 2), box(13, 0, 0, 14, 16, 3), box(12, 0, 0, 13, 16, 4), box(11, 0, 0, 12, 16, 5), box(10, 0, 0, 11, 16, 6), box(9, 0, 0, 10, 16, 7), box(8, 0, 0, 9, 16, 8),
					box(7, 0, 0, 8, 16, 9), box(6, 0, 0, 7, 16, 10), box(5, 0, 0, 6, 16, 11), box(4, 0, 0, 5, 16, 12), box(3, 0, 0, 4, 16, 13), box(2, 0, 0, 3, 16, 14), box(1, 0, 0, 2, 16, 15), box(0, 0, 0, 1, 16, 16));
			case WEST -> Shapes.or(box(0, 0, 15, 1, 16, 16), box(1, 0, 14, 2, 16, 16), box(2, 0, 13, 3, 16, 16), box(3, 0, 12, 4, 16, 16), box(4, 0, 11, 5, 16, 16), box(5, 0, 10, 6, 16, 16), box(6, 0, 9, 7, 16, 16), box(7, 0, 8, 8, 16, 16),
					box(8, 0, 7, 9, 16, 16), box(9, 0, 6, 10, 16, 16), box(10, 0, 5, 11, 16, 16), box(11, 0, 4, 12, 16, 16), box(12, 0, 3, 13, 16, 16), box(13, 0, 2, 14, 16, 16), box(14, 0, 1, 15, 16, 16), box(15, 0, 0, 16, 16, 16));
		};
	}

	@Override
	protected void createBlockStateDefinition(StateDefinition.Builder<Block, BlockState> builder) {
		builder.add(FACING, WATERLOGGED);
	}

	@Override
	public BlockState getStateForPlacement(BlockPlaceContext context) {
		boolean flag = context.getLevel().getFluidState(context.getClickedPos()).getType() == Fluids.WATER;
		return this.defaultBlockState().setValue(FACING, context.getHorizontalDirection().getOpposite()).setValue(WATERLOGGED, flag);
	}

	public BlockState rotate(BlockState state, Rotation rot) {
		return state.setValue(FACING, rot.rotate(state.getValue(FACING)));
	}

	public BlockState mirror(BlockState state, Mirror mirrorIn) {
		return state.rotate(mirrorIn.getRotation(state.getValue(FACING)));
	}

	@Override
	public FluidState getFluidState(BlockState state) {
		return state.getValue(WATERLOGGED) ? Fluids.WATER.getSource(false) : super.getFluidState(state);
	}

	@Override
	public BlockState updateShape(BlockState state, Direction facing, BlockState facingState, LevelAccessor world, BlockPos currentPos, BlockPos facingPos) {
		if (state.getValue(WATERLOGGED)) {
			world.scheduleTick(currentPos, Fluids.WATER, Fluids.WATER.getTickDelay(world));
		}
		return super.updateShape(state, facing, facingState, world, currentPos, facingPos);
	}

	@Override
	public ItemStack getCloneItemStack(BlockState state, HitResult target, BlockGetter world, BlockPos pos, Player player) {
		return new ItemStack(ModsCityModModBlocks.ROAD_SLOPE.get());
	}

	@Override
	public List<ItemStack> getDrops(BlockState state, LootContext.Builder builder) {
		List<ItemStack> dropsOriginal = super.getDrops(state, builder);
		if (!dropsOriginal.isEmpty())
			return dropsOriginal;
		return Collections.singletonList(new ItemStack(ModsCityModModBlocks.ROAD_SLOPE.get()));
	}

	@OnlyIn(Dist.CLIENT)
	public static void registerRenderLayer() {
		ItemBlockRenderTypes.setRenderLayer(ModsCityModModBlocks.DIAGONAL_ROAD_SLOPE.get(), renderType -> renderType == RenderType.cutout());
	}
}
