
package net.mcreator.modscitymod.block;

import net.minecraftforge.api.distmarker.OnlyIn;
import net.minecraftforge.api.distmarker.Dist;

import net.minecraft.world.phys.shapes.VoxelShape;
import net.minecraft.world.phys.shapes.Shapes;
import net.minecraft.world.phys.shapes.CollisionContext;
import net.minecraft.world.phys.HitResult;
import net.minecraft.world.level.storage.loot.LootContext;
import net.minecraft.world.level.material.Material;
import net.minecraft.world.level.material.Fluids;
import net.minecraft.world.level.material.FluidState;
import net.minecraft.world.level.block.state.properties.DirectionProperty;
import net.minecraft.world.level.block.state.properties.BooleanProperty;
import net.minecraft.world.level.block.state.properties.BlockStateProperties;
import net.minecraft.world.level.block.state.StateDefinition;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.level.block.state.BlockBehaviour;
import net.minecraft.world.level.block.SoundType;
import net.minecraft.world.level.block.SimpleWaterloggedBlock;
import net.minecraft.world.level.block.Rotation;
import net.minecraft.world.level.block.Mirror;
import net.minecraft.world.level.block.HorizontalDirectionalBlock;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.LevelAccessor;
import net.minecraft.world.level.BlockGetter;
import net.minecraft.world.item.context.BlockPlaceContext;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.entity.player.Player;
import net.minecraft.core.Direction;
import net.minecraft.core.BlockPos;
import net.minecraft.client.renderer.RenderType;
import net.minecraft.client.renderer.ItemBlockRenderTypes;

import net.mcreator.modscitymod.init.ModsCityModModBlocks;

import java.util.List;
import java.util.Collections;

public class FlippedRoadSlopeBlock extends Block implements SimpleWaterloggedBlock {
	public static final DirectionProperty FACING = HorizontalDirectionalBlock.FACING;
	public static final BooleanProperty WATERLOGGED = BlockStateProperties.WATERLOGGED;

	public FlippedRoadSlopeBlock() {
		super(BlockBehaviour.Properties.of(Material.STONE).sound(SoundType.STONE).strength(1f, 10f).noOcclusion().isRedstoneConductor((bs, br, bp) -> false));
		this.registerDefaultState(this.stateDefinition.any().setValue(FACING, Direction.NORTH).setValue(WATERLOGGED, false));
	}

	@Override
	public boolean skipRendering(BlockState state, BlockState adjacentBlockState, Direction side) {
		return adjacentBlockState.getBlock() == this ? true : super.skipRendering(state, adjacentBlockState, side);
	}

	@Override
	public boolean propagatesSkylightDown(BlockState state, BlockGetter reader, BlockPos pos) {
		return state.getFluidState().isEmpty();
	}

	@Override
	public int getLightBlock(BlockState state, BlockGetter worldIn, BlockPos pos) {
		return 0;
	}

	@Override
	public VoxelShape getVisualShape(BlockState state, BlockGetter world, BlockPos pos, CollisionContext context) {
		return Shapes.empty();
	}

	@Override
	public VoxelShape getShape(BlockState state, BlockGetter world, BlockPos pos, CollisionContext context) {
		return switch (state.getValue(FACING)) {
			default -> Shapes.or(box(0, 0, 0, 16, 1, 1), box(0, 1, 0, 16, 2, 2), box(0, 2, 0, 16, 3, 3), box(0, 3, 0, 16, 4, 4), box(0, 4, 0, 16, 5, 5), box(0, 5, 0, 16, 6, 6), box(0, 6, 0, 16, 7, 7), box(0, 7, 0, 16, 8, 8), box(0, 8, 0, 16, 9, 9),
					box(0, 9, 0, 16, 10, 10), box(0, 10, 0, 16, 11, 11), box(0, 11, 0, 16, 12, 12), box(0, 12, 0, 16, 13, 13), box(0, 13, 0, 16, 14, 14), box(0, 14, 0, 16, 15, 15), box(0, 15, 0, 16, 16, 16));
			case NORTH -> Shapes.or(box(0, 0, 15, 16, 1, 16), box(0, 1, 14, 16, 2, 16), box(0, 2, 13, 16, 3, 16), box(0, 3, 12, 16, 4, 16), box(0, 4, 11, 16, 5, 16), box(0, 5, 10, 16, 6, 16), box(0, 6, 9, 16, 7, 16), box(0, 7, 8, 16, 8, 16),
					box(0, 8, 7, 16, 9, 16), box(0, 9, 6, 16, 10, 16), box(0, 10, 5, 16, 11, 16), box(0, 11, 4, 16, 12, 16), box(0, 12, 3, 16, 13, 16), box(0, 13, 2, 16, 14, 16), box(0, 14, 1, 16, 15, 16), box(0, 15, 0, 16, 16, 16));
			case EAST -> Shapes.or(box(0, 0, 0, 1, 1, 16), box(0, 1, 0, 2, 2, 16), box(0, 2, 0, 3, 3, 16), box(0, 3, 0, 4, 4, 16), box(0, 4, 0, 5, 5, 16), box(0, 5, 0, 6, 6, 16), box(0, 6, 0, 7, 7, 16), box(0, 7, 0, 8, 8, 16), box(0, 8, 0, 9, 9, 16),
					box(0, 9, 0, 10, 10, 16), box(0, 10, 0, 11, 11, 16), box(0, 11, 0, 12, 12, 16), box(0, 12, 0, 13, 13, 16), box(0, 13, 0, 14, 14, 16), box(0, 14, 0, 15, 15, 16), box(0, 15, 0, 16, 16, 16));
			case WEST -> Shapes.or(box(15, 0, 0, 16, 1, 16), box(14, 1, 0, 16, 2, 16), box(13, 2, 0, 16, 3, 16), box(12, 3, 0, 16, 4, 16), box(11, 4, 0, 16, 5, 16), box(10, 5, 0, 16, 6, 16), box(9, 6, 0, 16, 7, 16), box(8, 7, 0, 16, 8, 16),
					box(7, 8, 0, 16, 9, 16), box(6, 9, 0, 16, 10, 16), box(5, 10, 0, 16, 11, 16), box(4, 11, 0, 16, 12, 16), box(3, 12, 0, 16, 13, 16), box(2, 13, 0, 16, 14, 16), box(1, 14, 0, 16, 15, 16), box(0, 15, 0, 16, 16, 16));
		};
	}

	@Override
	protected void createBlockStateDefinition(StateDefinition.Builder<Block, BlockState> builder) {
		builder.add(FACING, WATERLOGGED);
	}

	@Override
	public BlockState getStateForPlacement(BlockPlaceContext context) {
		boolean flag = context.getLevel().getFluidState(context.getClickedPos()).getType() == Fluids.WATER;
		return this.defaultBlockState().setValue(FACING, context.getHorizontalDirection().getOpposite()).setValue(WATERLOGGED, flag);
	}

	public BlockState rotate(BlockState state, Rotation rot) {
		return state.setValue(FACING, rot.rotate(state.getValue(FACING)));
	}

	public BlockState mirror(BlockState state, Mirror mirrorIn) {
		return state.rotate(mirrorIn.getRotation(state.getValue(FACING)));
	}

	@Override
	public FluidState getFluidState(BlockState state) {
		return state.getValue(WATERLOGGED) ? Fluids.WATER.getSource(false) : super.getFluidState(state);
	}

	@Override
	public BlockState updateShape(BlockState state, Direction facing, BlockState facingState, LevelAccessor world, BlockPos currentPos, BlockPos facingPos) {
		if (state.getValue(WATERLOGGED)) {
			world.scheduleTick(currentPos, Fluids.WATER, Fluids.WATER.getTickDelay(world));
		}
		return super.updateShape(state, facing, facingState, world, currentPos, facingPos);
	}

	@Override
	public ItemStack getCloneItemStack(BlockState state, HitResult target, BlockGetter world, BlockPos pos, Player player) {
		return new ItemStack(ModsCityModModBlocks.ROAD_SLOPE.get());
	}

	@Override
	public List<ItemStack> getDrops(BlockState state, LootContext.Builder builder) {
		List<ItemStack> dropsOriginal = super.getDrops(state, builder);
		if (!dropsOriginal.isEmpty())
			return dropsOriginal;
		return Collections.singletonList(new ItemStack(ModsCityModModBlocks.ROAD_SLOPE.get()));
	}

	@OnlyIn(Dist.CLIENT)
	public static void registerRenderLayer() {
		ItemBlockRenderTypes.setRenderLayer(ModsCityModModBlocks.FLIPPED_ROAD_SLOPE.get(), renderType -> renderType == RenderType.cutout());
	}
}
