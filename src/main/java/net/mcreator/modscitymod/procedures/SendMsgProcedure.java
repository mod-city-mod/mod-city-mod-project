package net.mcreator.modscitymod.procedures;

import net.minecraft.world.level.LevelAccessor;
import net.minecraft.client.gui.components.EditBox;

import net.mcreator.modscitymod.network.ModsCityModModVariables;

import java.util.HashMap;

public class SendMsgProcedure {
	public static void execute(LevelAccessor world, HashMap guistate) {
		if (guistate == null)
			return;
		ModsCityModModVariables.WorldVariables.get(world).msg = guistate.containsKey("text:MsgInsert") ? ((EditBox) guistate.get("text:MsgInsert")).getValue() : "";
		ModsCityModModVariables.WorldVariables.get(world).syncData(world);
		if (guistate.get("text:msg") instanceof EditBox _tf)
			_tf.setValue(ModsCityModModVariables.WorldVariables.get(world).msg);
	}
}
