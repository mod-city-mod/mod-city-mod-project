package net.mcreator.modscitymod.procedures;

import net.minecraft.world.level.LevelAccessor;
import net.minecraft.core.BlockPos;

import net.mcreator.modscitymod.init.ModsCityModModBlocks;

public class FuelPumpNeighbourBlockChangesProcedure {
	public static void execute(LevelAccessor world, double x, double y, double z) {
		if (!((world.getBlockState(new BlockPos(x, y + 1, z))).getBlock() == ModsCityModModBlocks.FUEL_PUMP_TOP.get())) {
			world.destroyBlock(new BlockPos(x, y, z), false);
		}
	}
}
