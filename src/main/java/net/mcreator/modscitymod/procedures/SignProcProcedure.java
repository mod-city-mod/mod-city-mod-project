package net.mcreator.modscitymod.procedures;

import net.minecraft.world.item.ItemStack;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.entity.LivingEntity;
import net.minecraft.world.entity.Entity;
import net.minecraft.network.chat.TextComponent;
import net.minecraft.client.gui.components.EditBox;

import java.util.HashMap;

public class SignProcProcedure {
	public static void execute(Entity entity, HashMap guistate) {
		if (entity == null || guistate == null)
			return;
		(entity instanceof LivingEntity _livEnt ? _livEnt.getMainHandItem() : ItemStack.EMPTY).getOrCreateTag().putString("user", (guistate.containsKey("text:user") ? ((EditBox) guistate.get("text:user")).getValue() : ""));
		((entity instanceof LivingEntity _livEnt ? _livEnt.getMainHandItem() : ItemStack.EMPTY))
				.setHoverName(new TextComponent((((entity instanceof LivingEntity _livEnt ? _livEnt.getMainHandItem() : ItemStack.EMPTY).getOrCreateTag().getString("user")) + "'s Credit Card")));
		if (entity instanceof Player _player)
			_player.closeContainer();
	}
}
