package net.mcreator.modscitymod.procedures;

import org.checkerframework.checker.units.qual.s;

import net.minecraft.client.gui.components.EditBox;

import java.util.HashMap;

public class DivisionProcProcedure {
	public static void execute(HashMap guistate) {
		if (guistate == null)
			return;
		if (guistate.get("text:result") instanceof EditBox _tf)
			_tf.setValue((new java.text.DecimalFormat("##.##").format(new Object() {
				double convert(String s) {
					try {
						return Double.parseDouble(s.trim());
					} catch (Exception e) {
					}
					return 0;
				}
			}.convert(guistate.containsKey("text:calc1") ? ((EditBox) guistate.get("text:calc1")).getValue() : "") / new Object() {
				double convert(String s) {
					try {
						return Double.parseDouble(s.trim());
					} catch (Exception e) {
					}
					return 0;
				}
			}.convert(guistate.containsKey("text:calc2") ? ((EditBox) guistate.get("text:calc2")).getValue() : ""))));
	}
}
