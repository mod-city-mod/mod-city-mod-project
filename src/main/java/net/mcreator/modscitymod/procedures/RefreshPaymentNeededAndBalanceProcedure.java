package net.mcreator.modscitymod.procedures;

import net.minecraft.world.level.LevelAccessor;
import net.minecraft.world.entity.Entity;

import java.util.HashMap;

public class RefreshPaymentNeededAndBalanceProcedure {
	public static void execute(LevelAccessor world, double x, double y, double z, Entity entity, HashMap guistate) {
		if (entity == null || guistate == null)
			return;
		RefreshBalanceProcedure.execute(world, entity, guistate);
		RefreshPaymentNeededProcedure.execute(world, x, y, z, guistate);
	}
}
