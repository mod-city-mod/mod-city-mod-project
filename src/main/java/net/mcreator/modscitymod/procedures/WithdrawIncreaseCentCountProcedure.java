package net.mcreator.modscitymod.procedures;

import org.checkerframework.checker.units.qual.s;

import net.minecraft.client.gui.components.EditBox;

import java.util.HashMap;

public class WithdrawIncreaseCentCountProcedure {
	public static void execute(HashMap guistate) {
		if (guistate == null)
			return;
		if (new Object() {
			double convert(String s) {
				try {
					return Double.parseDouble(s.trim());
				} catch (Exception e) {
				}
				return 0;
			}
		}.convert(guistate.containsKey("text:cents") ? ((EditBox) guistate.get("text:cents")).getValue() : "") < 90) {
			if (guistate.get("text:cents") instanceof EditBox _tf)
				_tf.setValue((new java.text.DecimalFormat("##.##").format(new Object() {
					double convert(String s) {
						try {
							return Double.parseDouble(s.trim());
						} catch (Exception e) {
						}
						return 0;
					}
				}.convert(guistate.containsKey("text:cents") ? ((EditBox) guistate.get("text:cents")).getValue() : "") + 5)));
		}
	}
}
