package net.mcreator.modscitymod.procedures;

import org.checkerframework.checker.units.qual.s;

import net.minecraft.world.level.LevelAccessor;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.entity.LivingEntity;
import net.minecraft.world.entity.Entity;
import net.minecraft.network.chat.TextComponent;
import net.minecraft.client.gui.components.EditBox;

import net.mcreator.modscitymod.network.ModsCityModModVariables;
import net.mcreator.modscitymod.init.ModsCityModModItems;

import java.util.HashMap;

public class PayWithCardProcedure {
	public static void execute(LevelAccessor world, Entity entity, HashMap guistate) {
		if (entity == null || guistate == null)
			return;
		if ((entity instanceof LivingEntity _livEnt ? _livEnt.getMainHandItem() : ItemStack.EMPTY).getItem() == ModsCityModModItems.CREDIT_CARD.get()
				|| (entity instanceof LivingEntity _livEnt ? _livEnt.getOffhandItem() : ItemStack.EMPTY).getItem() == ModsCityModModItems.CREDIT_CARD.get()) {
			(entity instanceof LivingEntity _livEnt ? _livEnt.getMainHandItem() : ItemStack.EMPTY).getOrCreateTag().putString("balance", (new java.text.DecimalFormat("##.##").format(new Object() {
				double convert(String s) {
					try {
						return Double.parseDouble(s.trim());
					} catch (Exception e) {
					}
					return 0;
				}
			}.convert((entity instanceof LivingEntity _livEnt ? _livEnt.getMainHandItem() : ItemStack.EMPTY).getOrCreateTag().getString("balance")) - ModsCityModModVariables.WorldVariables.get(world).pay)));
			if (guistate.get("text:balance") instanceof EditBox _tf)
				_tf.setValue(((entity instanceof LivingEntity _livEnt ? _livEnt.getMainHandItem() : ItemStack.EMPTY).getOrCreateTag().getString("balance")));
			ModsCityModModVariables.WorldVariables.get(world).pay = 0;
			ModsCityModModVariables.WorldVariables.get(world).syncData(world);
		} else {
			if (entity instanceof Player _player && !_player.level.isClientSide())
				_player.displayClientMessage(new TextComponent("Credit card not recognized"), (false));
		}
	}
}
