package net.mcreator.modscitymod.procedures;

import net.minecraft.world.level.LevelAccessor;
import net.minecraft.world.entity.Entity;
import net.minecraft.tags.TagKey;
import net.minecraft.server.level.ServerPlayer;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.core.Registry;
import net.minecraft.core.BlockPos;

import net.mcreator.modscitymod.init.ModsCityModModBlocks;

public class VehicleVoidEntityCollidesInTheBlockProcedure {
	public static void execute(LevelAccessor world, double x, double y, double z, Entity entity) {
		if (entity == null)
			return;
		boolean found = false;
		double sx = 0;
		double sy = 0;
		double sz = 0;
		double foundx = 0;
		double foundy = 0;
		double foundz = 0;
		if (entity.getType().is(TagKey.create(Registry.ENTITY_TYPE_REGISTRY, new ResourceLocation("mods_city:vehicles")))) {
			sx = -5;
			found = false;
			for (int index0 = 0; index0 < (int) (10); index0++) {
				sy = -5;
				for (int index1 = 0; index1 < (int) (10); index1++) {
					sz = -5;
					for (int index2 = 0; index2 < (int) (10); index2++) {
						if ((world.getBlockState(new BlockPos(x + sx, y + sy, z + sz))).getBlock() == ModsCityModModBlocks.VEHICLE_CHECKPOINT.get()) {
							found = true;
							foundx = x + sx;
							foundy = y + sy;
							foundz = z + sz;
						}
						sz = sz + 1;
					}
					sy = sy + 1;
				}
				sx = sx + 1;
			}
			if (found == true) {
				{
					Entity _ent = entity;
					_ent.teleportTo(foundx, foundy, foundz);
					if (_ent instanceof ServerPlayer _serverPlayer)
						_serverPlayer.connection.teleport(foundx, foundy, foundz, _ent.getYRot(), _ent.getXRot());
				}
			}
		}
	}
}
