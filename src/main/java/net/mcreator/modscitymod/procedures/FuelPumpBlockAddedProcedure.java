package net.mcreator.modscitymod.procedures;

import net.minecraft.world.level.block.Blocks;
import net.minecraft.world.level.LevelAccessor;
import net.minecraft.core.BlockPos;

import net.mcreator.modscitymod.init.ModsCityModModBlocks;

public class FuelPumpBlockAddedProcedure {
	public static boolean execute(LevelAccessor world, double x, double y, double z) {
		boolean FuelPumpPlaceable = false;
		if ((world.getBlockState(new BlockPos(x, y + 1, z))).getBlock() == Blocks.AIR || (world.getBlockState(new BlockPos(x, y + 1, z))).getBlock() == Blocks.WATER
				|| (world.getBlockState(new BlockPos(x, y + 1, z))).getBlock() == ModsCityModModBlocks.FUEL_PUMP_TOP.get()) {
			world.setBlock(new BlockPos(x, y + 1, z), ModsCityModModBlocks.FUEL_PUMP_TOP.get().defaultBlockState(), 3);
			FuelPumpPlaceable = true;
		} else {
			FuelPumpPlaceable = false;
		}
		return FuelPumpPlaceable;
	}
}
