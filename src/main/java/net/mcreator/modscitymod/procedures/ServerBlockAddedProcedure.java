package net.mcreator.modscitymod.procedures;

import net.minecraftforge.server.ServerLifecycleHooks;

import net.minecraft.world.phys.Vec3;
import net.minecraft.world.phys.AABB;
import net.minecraft.world.level.LevelAccessor;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.entity.Entity;
import net.minecraft.server.level.ServerLevel;
import net.minecraft.server.MinecraftServer;
import net.minecraft.network.chat.TextComponent;
import net.minecraft.network.chat.ChatType;
import net.minecraft.core.particles.ParticleTypes;
import net.minecraft.Util;

import java.util.Comparator;

public class ServerBlockAddedProcedure {
	public static void execute(LevelAccessor world, double x, double y, double z) {
		if (x == world.getLevelData().getXSpawn() && y == -59 && z == world.getLevelData().getZSpawn()) {
			if (!world.isClientSide()) {
				MinecraftServer _mcserv = ServerLifecycleHooks.getCurrentServer();
				if (_mcserv != null)
					_mcserv.getPlayerList().broadcastMessage(new TextComponent("Server Placed!"), ChatType.SYSTEM, Util.NIL_UUID);
			}
			if (world instanceof ServerLevel _level)
				_level.sendParticles(ParticleTypes.END_ROD, x, y, z, 20, 1, 1, 1, 0);
		} else {
			if (((Entity) world.getEntitiesOfClass(Player.class, AABB.ofSize(new Vec3(x, y, z), 4, 4, 4), e -> true).stream().sorted(new Object() {
				Comparator<Entity> compareDistOf(double _x, double _y, double _z) {
					return Comparator.comparingDouble(_entcnd -> _entcnd.distanceToSqr(_x, _y, _z));
				}
			}.compareDistOf(x, y, z)).findFirst().orElse(null)) instanceof Player _player && !_player.level.isClientSide())
				_player.displayClientMessage(new TextComponent(("Place me underground at world spawn: "
						+ (("x = " + new java.text.DecimalFormat("##").format(world.getLevelData().getXSpawn())) + ", y = -59, " + ("z = " + new java.text.DecimalFormat("##").format(world.getLevelData().getZSpawn()))))), (false));
		}
	}
}
