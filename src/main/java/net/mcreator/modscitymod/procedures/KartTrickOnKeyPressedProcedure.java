package net.mcreator.modscitymod.procedures;

import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.event.TickEvent;
import net.minecraftforge.common.MinecraftForge;

import net.minecraft.world.level.LevelAccessor;
import net.minecraft.world.entity.LivingEntity;
import net.minecraft.world.entity.Entity;
import net.minecraft.world.effect.MobEffects;
import net.minecraft.world.effect.MobEffectInstance;
import net.minecraft.tags.TagKey;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.core.Registry;
import net.minecraft.core.BlockPos;

import net.mcreator.modscitymod.init.ModsCityModModBlocks;

public class KartTrickOnKeyPressedProcedure {
	public static void execute(LevelAccessor world, double x, double y, double z, Entity entity) {
		if (entity == null)
			return;
		if (entity.isPassenger()) {
			if ((entity.getVehicle()).getType().is(TagKey.create(Registry.ENTITY_TYPE_REGISTRY, new ResourceLocation("mods_city:vehicles")))) {
				if ((world.getBlockState(new BlockPos(x, y, z))).getBlock() == ModsCityModModBlocks.KART_TRICK_BLOCK.get() || (world.getBlockState(new BlockPos(x, y + 0.5, z))).getBlock() == ModsCityModModBlocks.KART_TRICK_BLOCK.get()
						|| (world.getBlockState(new BlockPos(x, y + -0.5, z))).getBlock() == ModsCityModModBlocks.KART_TRICK_BLOCK.get()) {
					{
						Entity _ent = entity;
						if (!_ent.level.isClientSide() && _ent.getServer() != null)
							_ent.getServer().getCommands().performCommand(_ent.createCommandSourceStack().withSuppressedOutput().withPermission(4), "/particle minecraft:flash ~ ~ ~");
					}
					{
						Entity _ent = entity;
						if (!_ent.level.isClientSide() && _ent.getServer() != null)
							_ent.getServer().getCommands().performCommand(_ent.createCommandSourceStack().withSuppressedOutput().withPermission(4), "/playsound entity.experience_orb.pickup block @s");
					}
					if ((entity.getVehicle()) instanceof LivingEntity _entity)
						_entity.addEffect(new MobEffectInstance(MobEffects.LEVITATION, 5, 10, (false), (false)));
					new Object() {
						private int ticks = 0;
						private float waitTicks;
						private LevelAccessor world;

						public void start(LevelAccessor world, int waitTicks) {
							this.waitTicks = waitTicks;
							MinecraftForge.EVENT_BUS.register(this);
							this.world = world;
						}

						@SubscribeEvent
						public void tick(TickEvent.ServerTickEvent event) {
							if (event.phase == TickEvent.Phase.END) {
								this.ticks += 1;
								if (this.ticks >= this.waitTicks)
									run();
							}
						}

						private void run() {
							if ((entity.getVehicle()) instanceof LivingEntity _entity)
								_entity.addEffect(new MobEffectInstance(MobEffects.MOVEMENT_SPEED, 20, 2, (false), (false)));
							for (int index0 = 0; index0 < (int) (20); index0++) {
								new Object() {
									private int ticks = 0;
									private float waitTicks;
									private LevelAccessor world;

									public void start(LevelAccessor world, int waitTicks) {
										this.waitTicks = waitTicks;
										MinecraftForge.EVENT_BUS.register(this);
										this.world = world;
									}

									@SubscribeEvent
									public void tick(TickEvent.ServerTickEvent event) {
										if (event.phase == TickEvent.Phase.END) {
											this.ticks += 1;
											if (this.ticks >= this.waitTicks)
												run();
										}
									}

									private void run() {
										{
											Entity _ent = (entity.getVehicle());
											if (!_ent.level.isClientSide() && _ent.getServer() != null)
												_ent.getServer().getCommands().performCommand(_ent.createCommandSourceStack().withSuppressedOutput().withPermission(4), "particle minecraft:flame ^0.3 ^0.6 ^-1");
										}
										MinecraftForge.EVENT_BUS.unregister(this);
									}
								}.start(world, 1);
							}
							MinecraftForge.EVENT_BUS.unregister(this);
						}
					}.start(world, 5);
				}
			}
		}
	}
}
