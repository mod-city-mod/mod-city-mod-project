package net.mcreator.modscitymod.procedures;

import net.minecraft.world.entity.Entity;

public class KartParticlesProcProcedure {
	public static void execute(Entity entity) {
		if (entity == null)
			return;
		if (entity.getDeltaMovement().x() != 0 && entity.getDeltaMovement().z() != 0) {
			{
				Entity _ent = entity;
				if (!_ent.level.isClientSide() && _ent.getServer() != null)
					_ent.getServer().getCommands().performCommand(_ent.createCommandSourceStack().withSuppressedOutput().withPermission(4), "particle minecraft:smoke ^0.3 ^0.6 ^-1");
			}
		}
	}
}
