package net.mcreator.modscitymod.procedures;

import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.event.TickEvent;
import net.minecraftforge.common.MinecraftForge;

import net.minecraft.world.level.LevelAccessor;
import net.minecraft.world.entity.Entity;

import java.util.HashMap;

public class WithdrawGUIFixProcedure {
	public static void execute(LevelAccessor world, Entity entity, HashMap guistate) {
		if (entity == null || guistate == null)
			return;
		new Object() {
			private int ticks = 0;
			private float waitTicks;
			private LevelAccessor world;

			public void start(LevelAccessor world, int waitTicks) {
				this.waitTicks = waitTicks;
				MinecraftForge.EVENT_BUS.register(this);
				this.world = world;
			}

			@SubscribeEvent
			public void tick(TickEvent.ServerTickEvent event) {
				if (event.phase == TickEvent.Phase.END) {
					this.ticks += 1;
					if (this.ticks >= this.waitTicks)
						run();
				}
			}

			private void run() {
				WithdrawIncreaseBuckCountProcedure.execute(world, guistate);
				WithdrawIncreaseBuckCountProcedure.execute(world, guistate);
				WithdrawDecreaseBuckCountProcedure.execute(guistate);
				WithdrawDecreaseBuckCountProcedure.execute(guistate);
				WithdrawIncreaseCentCountProcedure.execute(guistate);
				WithdrawIncreaseCentCountProcedure.execute(guistate);
				WithdrawDecreaseCentCountProcedure.execute(guistate);
				WithdrawDecreaseCentCountProcedure.execute(guistate);
				RefreshBalanceProcedure.execute(world, entity, guistate);
				MinecraftForge.EVENT_BUS.unregister(this);
			}
		}.start(world, 5);
	}
}
