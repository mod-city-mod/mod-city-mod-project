
package net.mcreator.modscitymod.client.gui;

import net.minecraft.world.level.Level;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.entity.player.Inventory;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.network.chat.TranslatableComponent;
import net.minecraft.network.chat.Component;
import net.minecraft.client.gui.screens.inventory.AbstractContainerScreen;
import net.minecraft.client.gui.components.Button;
import net.minecraft.client.Minecraft;

import net.mcreator.modscitymod.world.inventory.GamemodesMenu;
import net.mcreator.modscitymod.network.GamemodesButtonMessage;
import net.mcreator.modscitymod.ModsCityModMod;

import java.util.HashMap;

import com.mojang.blaze3d.vertex.PoseStack;
import com.mojang.blaze3d.systems.RenderSystem;

public class GamemodesScreen extends AbstractContainerScreen<GamemodesMenu> {
	private final static HashMap<String, Object> guistate = GamemodesMenu.guistate;
	private final Level world;
	private final int x, y, z;
	private final Player entity;
	Button button_survival;
	Button button_creative;
	Button button_spectator;
	Button button_adventure;
	Button button_x;

	public GamemodesScreen(GamemodesMenu container, Inventory inventory, Component text) {
		super(container, inventory, text);
		this.world = container.world;
		this.x = container.x;
		this.y = container.y;
		this.z = container.z;
		this.entity = container.entity;
		this.imageWidth = 175;
		this.imageHeight = 131;
	}

	private static final ResourceLocation texture = new ResourceLocation("mods_city_mod:textures/screens/gamemodes.png");

	@Override
	public void render(PoseStack ms, int mouseX, int mouseY, float partialTicks) {
		this.renderBackground(ms);
		super.render(ms, mouseX, mouseY, partialTicks);
		this.renderTooltip(ms, mouseX, mouseY);
	}

	@Override
	protected void renderBg(PoseStack ms, float partialTicks, int gx, int gy) {
		RenderSystem.setShaderColor(1, 1, 1, 1);
		RenderSystem.enableBlend();
		RenderSystem.defaultBlendFunc();
		RenderSystem.setShaderTexture(0, texture);
		this.blit(ms, this.leftPos, this.topPos, 0, 0, this.imageWidth, this.imageHeight, this.imageWidth, this.imageHeight);

		RenderSystem.setShaderTexture(0, new ResourceLocation("mods_city_mod:textures/screens/gamemodeswitchersmall.png"));
		this.blit(ms, this.leftPos + 4, this.topPos + 0, 0, 0, 18, 18, 18, 18);

		RenderSystem.disableBlend();
	}

	@Override
	public boolean keyPressed(int key, int b, int c) {
		if (key == 256) {
			this.minecraft.player.closeContainer();
			return true;
		}
		return super.keyPressed(key, b, c);
	}

	@Override
	public void containerTick() {
		super.containerTick();
	}

	@Override
	protected void renderLabels(PoseStack poseStack, int mouseX, int mouseY) {
		this.font.draw(poseStack, new TranslatableComponent("gui.mods_city_mod.gamemodes.label_gamemodes"), 24, 6, -12829636);
	}

	@Override
	public void onClose() {
		super.onClose();
		Minecraft.getInstance().keyboardHandler.setSendRepeatsToGui(false);
	}

	@Override
	public void init() {
		super.init();
		this.minecraft.keyboardHandler.setSendRepeatsToGui(true);
		button_survival = new Button(this.leftPos + 11, this.topPos + 31, 67, 20, new TranslatableComponent("gui.mods_city_mod.gamemodes.button_survival"), e -> {
			if (true) {
				ModsCityModMod.PACKET_HANDLER.sendToServer(new GamemodesButtonMessage(0, x, y, z));
				GamemodesButtonMessage.handleButtonAction(entity, 0, x, y, z);
			}
		});
		guistate.put("button:button_survival", button_survival);
		this.addRenderableWidget(button_survival);
		button_creative = new Button(this.leftPos + 95, this.topPos + 31, 67, 20, new TranslatableComponent("gui.mods_city_mod.gamemodes.button_creative"), e -> {
			if (true) {
				ModsCityModMod.PACKET_HANDLER.sendToServer(new GamemodesButtonMessage(1, x, y, z));
				GamemodesButtonMessage.handleButtonAction(entity, 1, x, y, z);
			}
		});
		guistate.put("button:button_creative", button_creative);
		this.addRenderableWidget(button_creative);
		button_spectator = new Button(this.leftPos + 11, this.topPos + 56, 67, 20, new TranslatableComponent("gui.mods_city_mod.gamemodes.button_spectator"), e -> {
			if (true) {
				ModsCityModMod.PACKET_HANDLER.sendToServer(new GamemodesButtonMessage(2, x, y, z));
				GamemodesButtonMessage.handleButtonAction(entity, 2, x, y, z);
			}
		});
		guistate.put("button:button_spectator", button_spectator);
		this.addRenderableWidget(button_spectator);
		button_adventure = new Button(this.leftPos + 95, this.topPos + 56, 67, 20, new TranslatableComponent("gui.mods_city_mod.gamemodes.button_adventure"), e -> {
			if (true) {
				ModsCityModMod.PACKET_HANDLER.sendToServer(new GamemodesButtonMessage(3, x, y, z));
				GamemodesButtonMessage.handleButtonAction(entity, 3, x, y, z);
			}
		});
		guistate.put("button:button_adventure", button_adventure);
		this.addRenderableWidget(button_adventure);
		button_x = new Button(this.leftPos + 152, this.topPos + 3, 20, 20, new TranslatableComponent("gui.mods_city_mod.gamemodes.button_x"), e -> {
			if (true) {
				ModsCityModMod.PACKET_HANDLER.sendToServer(new GamemodesButtonMessage(4, x, y, z));
				GamemodesButtonMessage.handleButtonAction(entity, 4, x, y, z);
			}
		});
		guistate.put("button:button_x", button_x);
		this.addRenderableWidget(button_x);
	}
}
