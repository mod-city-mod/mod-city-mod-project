
package net.mcreator.modscitymod.client.gui;

import net.minecraft.world.level.Level;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.entity.player.Inventory;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.network.chat.TranslatableComponent;
import net.minecraft.network.chat.Component;
import net.minecraft.client.gui.screens.inventory.AbstractContainerScreen;
import net.minecraft.client.gui.components.EditBox;
import net.minecraft.client.gui.components.Button;
import net.minecraft.client.Minecraft;

import net.mcreator.modscitymod.world.inventory.AtmGUIMenu;
import net.mcreator.modscitymod.network.AtmGUIButtonMessage;
import net.mcreator.modscitymod.ModsCityModMod;

import java.util.HashMap;

import com.mojang.blaze3d.vertex.PoseStack;
import com.mojang.blaze3d.systems.RenderSystem;

public class AtmGUIScreen extends AbstractContainerScreen<AtmGUIMenu> {
	private final static HashMap<String, Object> guistate = AtmGUIMenu.guistate;
	private final Level world;
	private final int x, y, z;
	private final Player entity;
	EditBox balance;
	EditBox username;
	Button button_log_in;
	Button button_deposit;
	Button button_withdraw;

	public AtmGUIScreen(AtmGUIMenu container, Inventory inventory, Component text) {
		super(container, inventory, text);
		this.world = container.world;
		this.x = container.x;
		this.y = container.y;
		this.z = container.z;
		this.entity = container.entity;
		this.imageWidth = 176;
		this.imageHeight = 166;
	}

	private static final ResourceLocation texture = new ResourceLocation("mods_city_mod:textures/screens/atm_gui.png");

	@Override
	public void render(PoseStack ms, int mouseX, int mouseY, float partialTicks) {
		this.renderBackground(ms);
		super.render(ms, mouseX, mouseY, partialTicks);
		this.renderTooltip(ms, mouseX, mouseY);
		balance.render(ms, mouseX, mouseY, partialTicks);
		username.render(ms, mouseX, mouseY, partialTicks);
	}

	@Override
	protected void renderBg(PoseStack ms, float partialTicks, int gx, int gy) {
		RenderSystem.setShaderColor(1, 1, 1, 1);
		RenderSystem.enableBlend();
		RenderSystem.defaultBlendFunc();
		RenderSystem.setShaderTexture(0, texture);
		this.blit(ms, this.leftPos, this.topPos, 0, 0, this.imageWidth, this.imageHeight, this.imageWidth, this.imageHeight);
		RenderSystem.disableBlend();
	}

	@Override
	public boolean keyPressed(int key, int b, int c) {
		if (key == 256) {
			this.minecraft.player.closeContainer();
			return true;
		}
		if (balance.isFocused())
			return balance.keyPressed(key, b, c);
		if (username.isFocused())
			return username.keyPressed(key, b, c);
		return super.keyPressed(key, b, c);
	}

	@Override
	public void containerTick() {
		super.containerTick();
		balance.tick();
		username.tick();
	}

	@Override
	protected void renderLabels(PoseStack poseStack, int mouseX, int mouseY) {
		this.font.draw(poseStack, new TranslatableComponent("gui.mods_city_mod.atm_gui.label_balance"), 6, 52, -12829636);
		this.font.draw(poseStack, new TranslatableComponent("gui.mods_city_mod.atm_gui.label_user"), 6, 25, -12829636);
	}

	@Override
	public void onClose() {
		super.onClose();
		Minecraft.getInstance().keyboardHandler.setSendRepeatsToGui(false);
	}

	@Override
	public void init() {
		super.init();
		this.minecraft.keyboardHandler.setSendRepeatsToGui(true);
		balance = new EditBox(this.font, this.leftPos + 51, this.topPos + 43, 120, 20, new TranslatableComponent("gui.mods_city_mod.atm_gui.balance")) {
			{
				setSuggestion(new TranslatableComponent("gui.mods_city_mod.atm_gui.balance").getString());
			}

			@Override
			public void insertText(String text) {
				super.insertText(text);
				if (getValue().isEmpty())
					setSuggestion(new TranslatableComponent("gui.mods_city_mod.atm_gui.balance").getString());
				else
					setSuggestion(null);
			}

			@Override
			public void moveCursorTo(int pos) {
				super.moveCursorTo(pos);
				if (getValue().isEmpty())
					setSuggestion(new TranslatableComponent("gui.mods_city_mod.atm_gui.balance").getString());
				else
					setSuggestion(null);
			}
		};
		balance.setMaxLength(32767);
		guistate.put("text:balance", balance);
		this.addWidget(this.balance);
		username = new EditBox(this.font, this.leftPos + 51, this.topPos + 16, 120, 20, new TranslatableComponent("gui.mods_city_mod.atm_gui.username")) {
			{
				setSuggestion(new TranslatableComponent("gui.mods_city_mod.atm_gui.username").getString());
			}

			@Override
			public void insertText(String text) {
				super.insertText(text);
				if (getValue().isEmpty())
					setSuggestion(new TranslatableComponent("gui.mods_city_mod.atm_gui.username").getString());
				else
					setSuggestion(null);
			}

			@Override
			public void moveCursorTo(int pos) {
				super.moveCursorTo(pos);
				if (getValue().isEmpty())
					setSuggestion(new TranslatableComponent("gui.mods_city_mod.atm_gui.username").getString());
				else
					setSuggestion(null);
			}
		};
		username.setMaxLength(32767);
		guistate.put("text:username", username);
		this.addWidget(this.username);
		button_log_in = new Button(this.leftPos + 60, this.topPos + 70, 56, 20, new TranslatableComponent("gui.mods_city_mod.atm_gui.button_log_in"), e -> {
			if (true) {
				ModsCityModMod.PACKET_HANDLER.sendToServer(new AtmGUIButtonMessage(0, x, y, z));
				AtmGUIButtonMessage.handleButtonAction(entity, 0, x, y, z);
			}
		});
		guistate.put("button:button_log_in", button_log_in);
		this.addRenderableWidget(button_log_in);
		button_deposit = new Button(this.leftPos + 16, this.topPos + 115, 67, 20, new TranslatableComponent("gui.mods_city_mod.atm_gui.button_deposit"), e -> {
			if (true) {
				ModsCityModMod.PACKET_HANDLER.sendToServer(new AtmGUIButtonMessage(1, x, y, z));
				AtmGUIButtonMessage.handleButtonAction(entity, 1, x, y, z);
			}
		});
		guistate.put("button:button_deposit", button_deposit);
		this.addRenderableWidget(button_deposit);
		button_withdraw = new Button(this.leftPos + 96, this.topPos + 115, 67, 20, new TranslatableComponent("gui.mods_city_mod.atm_gui.button_withdraw"), e -> {
			if (true) {
				ModsCityModMod.PACKET_HANDLER.sendToServer(new AtmGUIButtonMessage(2, x, y, z));
				AtmGUIButtonMessage.handleButtonAction(entity, 2, x, y, z);
			}
		});
		guistate.put("button:button_withdraw", button_withdraw);
		this.addRenderableWidget(button_withdraw);
	}
}
