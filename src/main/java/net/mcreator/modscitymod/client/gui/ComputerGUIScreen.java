
package net.mcreator.modscitymod.client.gui;

import net.minecraft.world.level.Level;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.entity.player.Inventory;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.network.chat.TranslatableComponent;
import net.minecraft.network.chat.Component;
import net.minecraft.client.gui.screens.inventory.AbstractContainerScreen;
import net.minecraft.client.gui.components.ImageButton;
import net.minecraft.client.gui.components.Button;
import net.minecraft.client.Minecraft;

import net.mcreator.modscitymod.world.inventory.ComputerGUIMenu;
import net.mcreator.modscitymod.network.ComputerGUIButtonMessage;
import net.mcreator.modscitymod.ModsCityModMod;

import java.util.HashMap;

import com.mojang.blaze3d.vertex.PoseStack;
import com.mojang.blaze3d.systems.RenderSystem;

public class ComputerGUIScreen extends AbstractContainerScreen<ComputerGUIMenu> {
	private final static HashMap<String, Object> guistate = ComputerGUIMenu.guistate;
	private final Level world;
	private final int x, y, z;
	private final Player entity;
	Button button_interwebs;
	Button button_messages;
	ImageButton imagebutton_mods_start;
	ImageButton imagebutton_gamemodeswitchersmall;
	ImageButton imagebutton_gamemode_switcher_dock;

	public ComputerGUIScreen(ComputerGUIMenu container, Inventory inventory, Component text) {
		super(container, inventory, text);
		this.world = container.world;
		this.x = container.x;
		this.y = container.y;
		this.z = container.z;
		this.entity = container.entity;
		this.imageWidth = 256;
		this.imageHeight = 144;
	}

	private static final ResourceLocation texture = new ResourceLocation("mods_city_mod:textures/screens/computer_gui.png");

	@Override
	public void render(PoseStack ms, int mouseX, int mouseY, float partialTicks) {
		this.renderBackground(ms);
		super.render(ms, mouseX, mouseY, partialTicks);
		this.renderTooltip(ms, mouseX, mouseY);
	}

	@Override
	protected void renderBg(PoseStack ms, float partialTicks, int gx, int gy) {
		RenderSystem.setShaderColor(1, 1, 1, 1);
		RenderSystem.enableBlend();
		RenderSystem.defaultBlendFunc();
		RenderSystem.setShaderTexture(0, texture);
		this.blit(ms, this.leftPos, this.topPos, 0, 0, this.imageWidth, this.imageHeight, this.imageWidth, this.imageHeight);

		RenderSystem.setShaderTexture(0, new ResourceLocation("mods_city_mod:textures/screens/pc_wallpaper.png"));
		this.blit(ms, this.leftPos + 0, this.topPos + 0, 0, 0, 256, 144, 256, 144);

		RenderSystem.setShaderTexture(0, new ResourceLocation("mods_city_mod:textures/screens/pc_screen_template.png"));
		this.blit(ms, this.leftPos + 0, this.topPos + 0, 0, 0, 256, 144, 256, 144);

		RenderSystem.setShaderTexture(0, new ResourceLocation("mods_city_mod:textures/screens/messages.png"));
		this.blit(ms, this.leftPos + 163, this.topPos + 32, 0, 0, 12, 14, 12, 14);

		RenderSystem.setShaderTexture(0, new ResourceLocation("mods_city_mod:textures/screens/pc_dock.png"));
		this.blit(ms, this.leftPos + 0, this.topPos + 0, 0, 0, 256, 144, 256, 144);

		RenderSystem.disableBlend();
	}

	@Override
	public boolean keyPressed(int key, int b, int c) {
		if (key == 256) {
			this.minecraft.player.closeContainer();
			return true;
		}
		return super.keyPressed(key, b, c);
	}

	@Override
	public void containerTick() {
		super.containerTick();
	}

	@Override
	protected void renderLabels(PoseStack poseStack, int mouseX, int mouseY) {
		this.font.draw(poseStack, new TranslatableComponent("gui.mods_city_mod.computer_gui.label_mods_os"), 196, 71, -12829636);
		this.font.draw(poseStack, new TranslatableComponent("gui.mods_city_mod.computer_gui.label_game_modes"), 12, 50, -1);
	}

	@Override
	public void onClose() {
		super.onClose();
		Minecraft.getInstance().keyboardHandler.setSendRepeatsToGui(false);
	}

	@Override
	public void init() {
		super.init();
		this.minecraft.keyboardHandler.setSendRepeatsToGui(true);
		button_interwebs = new Button(this.leftPos + 55, this.topPos + 95, 72, 20, new TranslatableComponent("gui.mods_city_mod.computer_gui.button_interwebs"), e -> {
			if (true) {
				ModsCityModMod.PACKET_HANDLER.sendToServer(new ComputerGUIButtonMessage(0, x, y, z));
				ComputerGUIButtonMessage.handleButtonAction(entity, 0, x, y, z);
			}
		});
		guistate.put("button:button_interwebs", button_interwebs);
		this.addRenderableWidget(button_interwebs);
		button_messages = new Button(this.leftPos + 136, this.topPos + 50, 67, 20, new TranslatableComponent("gui.mods_city_mod.computer_gui.button_messages"), e -> {
			if (true) {
				ModsCityModMod.PACKET_HANDLER.sendToServer(new ComputerGUIButtonMessage(1, x, y, z));
				ComputerGUIButtonMessage.handleButtonAction(entity, 1, x, y, z);
			}
		});
		guistate.put("button:button_messages", button_messages);
		this.addRenderableWidget(button_messages);
		imagebutton_mods_start = new ImageButton(this.leftPos + 8, this.topPos + 128, 8, 8, 0, 0, 8, new ResourceLocation("mods_city_mod:textures/screens/atlas/imagebutton_mods_start.png"), 8, 16, e -> {
		});
		guistate.put("button:imagebutton_mods_start", imagebutton_mods_start);
		this.addRenderableWidget(imagebutton_mods_start);
		imagebutton_gamemodeswitchersmall = new ImageButton(this.leftPos + 19, this.topPos + 14, 36, 36, 0, 0, 36, new ResourceLocation("mods_city_mod:textures/screens/atlas/imagebutton_gamemodeswitchersmall.png"), 36, 72, e -> {
			if (true) {
				ModsCityModMod.PACKET_HANDLER.sendToServer(new ComputerGUIButtonMessage(3, x, y, z));
				ComputerGUIButtonMessage.handleButtonAction(entity, 3, x, y, z);
			}
		});
		guistate.put("button:imagebutton_gamemodeswitchersmall", imagebutton_gamemodeswitchersmall);
		this.addRenderableWidget(imagebutton_gamemodeswitchersmall);
		imagebutton_gamemode_switcher_dock = new ImageButton(this.leftPos + 21, this.topPos + 128, 8, 8, 0, 0, 8, new ResourceLocation("mods_city_mod:textures/screens/atlas/imagebutton_gamemode_switcher_dock.png"), 8, 16, e -> {
			if (true) {
				ModsCityModMod.PACKET_HANDLER.sendToServer(new ComputerGUIButtonMessage(4, x, y, z));
				ComputerGUIButtonMessage.handleButtonAction(entity, 4, x, y, z);
			}
		});
		guistate.put("button:imagebutton_gamemode_switcher_dock", imagebutton_gamemode_switcher_dock);
		this.addRenderableWidget(imagebutton_gamemode_switcher_dock);
	}
}
