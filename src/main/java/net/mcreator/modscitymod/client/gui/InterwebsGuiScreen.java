
package net.mcreator.modscitymod.client.gui;

import net.minecraft.world.level.Level;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.entity.player.Inventory;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.network.chat.TranslatableComponent;
import net.minecraft.network.chat.Component;
import net.minecraft.client.gui.screens.inventory.AbstractContainerScreen;
import net.minecraft.client.gui.components.EditBox;
import net.minecraft.client.gui.components.Button;
import net.minecraft.client.Minecraft;

import net.mcreator.modscitymod.world.inventory.InterwebsGuiMenu;
import net.mcreator.modscitymod.network.InterwebsGuiButtonMessage;
import net.mcreator.modscitymod.ModsCityModMod;

import java.util.HashMap;

import com.mojang.blaze3d.vertex.PoseStack;
import com.mojang.blaze3d.systems.RenderSystem;

public class InterwebsGuiScreen extends AbstractContainerScreen<InterwebsGuiMenu> {
	private final static HashMap<String, Object> guistate = InterwebsGuiMenu.guistate;
	private final Level world;
	private final int x, y, z;
	private final Player entity;
	EditBox url;
	Button button_bdscookiescom;
	Button button_x;
	Button button_go;

	public InterwebsGuiScreen(InterwebsGuiMenu container, Inventory inventory, Component text) {
		super(container, inventory, text);
		this.world = container.world;
		this.x = container.x;
		this.y = container.y;
		this.z = container.z;
		this.entity = container.entity;
		this.imageWidth = 175;
		this.imageHeight = 131;
	}

	private static final ResourceLocation texture = new ResourceLocation("mods_city_mod:textures/screens/interwebs_gui.png");

	@Override
	public void render(PoseStack ms, int mouseX, int mouseY, float partialTicks) {
		this.renderBackground(ms);
		super.render(ms, mouseX, mouseY, partialTicks);
		this.renderTooltip(ms, mouseX, mouseY);
		url.render(ms, mouseX, mouseY, partialTicks);
	}

	@Override
	protected void renderBg(PoseStack ms, float partialTicks, int gx, int gy) {
		RenderSystem.setShaderColor(1, 1, 1, 1);
		RenderSystem.enableBlend();
		RenderSystem.defaultBlendFunc();
		RenderSystem.setShaderTexture(0, texture);
		this.blit(ms, this.leftPos, this.topPos, 0, 0, this.imageWidth, this.imageHeight, this.imageWidth, this.imageHeight);

		RenderSystem.setShaderTexture(0, new ResourceLocation("mods_city_mod:textures/screens/browser_tabs.png"));
		this.blit(ms, this.leftPos + 0, this.topPos + 0, 0, 0, 175, 131, 175, 131);

		RenderSystem.disableBlend();
	}

	@Override
	public boolean keyPressed(int key, int b, int c) {
		if (key == 256) {
			this.minecraft.player.closeContainer();
			return true;
		}
		if (url.isFocused())
			return url.keyPressed(key, b, c);
		return super.keyPressed(key, b, c);
	}

	@Override
	public void containerTick() {
		super.containerTick();
		url.tick();
	}

	@Override
	protected void renderLabels(PoseStack poseStack, int mouseX, int mouseY) {
		this.font.draw(poseStack, new TranslatableComponent("gui.mods_city_mod.interwebs_gui.label_tab"), 5, 3, -12829636);
		this.font.draw(poseStack, new TranslatableComponent("gui.mods_city_mod.interwebs_gui.label_bookmarks"), 6, 92, -12829636);
	}

	@Override
	public void onClose() {
		super.onClose();
		Minecraft.getInstance().keyboardHandler.setSendRepeatsToGui(false);
	}

	@Override
	public void init() {
		super.init();
		this.minecraft.keyboardHandler.setSendRepeatsToGui(true);
		url = new EditBox(this.font, this.leftPos + 11, this.topPos + 43, 120, 20, new TranslatableComponent("gui.mods_city_mod.interwebs_gui.url")) {
			{
				setSuggestion(new TranslatableComponent("gui.mods_city_mod.interwebs_gui.url").getString());
			}

			@Override
			public void insertText(String text) {
				super.insertText(text);
				if (getValue().isEmpty())
					setSuggestion(new TranslatableComponent("gui.mods_city_mod.interwebs_gui.url").getString());
				else
					setSuggestion(null);
			}

			@Override
			public void moveCursorTo(int pos) {
				super.moveCursorTo(pos);
				if (getValue().isEmpty())
					setSuggestion(new TranslatableComponent("gui.mods_city_mod.interwebs_gui.url").getString());
				else
					setSuggestion(null);
			}
		};
		url.setMaxLength(32767);
		guistate.put("text:url", url);
		this.addWidget(this.url);
		button_bdscookiescom = new Button(this.leftPos + 5, this.topPos + 105, 98, 20, new TranslatableComponent("gui.mods_city_mod.interwebs_gui.button_bdscookiescom"), e -> {
			if (true) {
				ModsCityModMod.PACKET_HANDLER.sendToServer(new InterwebsGuiButtonMessage(0, x, y, z));
				InterwebsGuiButtonMessage.handleButtonAction(entity, 0, x, y, z);
			}
		});
		guistate.put("button:button_bdscookiescom", button_bdscookiescom);
		this.addRenderableWidget(button_bdscookiescom);
		button_x = new Button(this.leftPos + 152, this.topPos + 3, 20, 20, new TranslatableComponent("gui.mods_city_mod.interwebs_gui.button_x"), e -> {
			if (true) {
				ModsCityModMod.PACKET_HANDLER.sendToServer(new InterwebsGuiButtonMessage(1, x, y, z));
				InterwebsGuiButtonMessage.handleButtonAction(entity, 1, x, y, z);
			}
		});
		guistate.put("button:button_x", button_x);
		this.addRenderableWidget(button_x);
		button_go = new Button(this.leftPos + 132, this.topPos + 43, 30, 20, new TranslatableComponent("gui.mods_city_mod.interwebs_gui.button_go"), e -> {
			if (true) {
				ModsCityModMod.PACKET_HANDLER.sendToServer(new InterwebsGuiButtonMessage(2, x, y, z));
				InterwebsGuiButtonMessage.handleButtonAction(entity, 2, x, y, z);
			}
		});
		guistate.put("button:button_go", button_go);
		this.addRenderableWidget(button_go);
	}
}
