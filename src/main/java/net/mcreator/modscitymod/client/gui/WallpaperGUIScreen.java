
package net.mcreator.modscitymod.client.gui;

import net.minecraft.world.level.Level;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.entity.player.Inventory;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.network.chat.TranslatableComponent;
import net.minecraft.network.chat.Component;
import net.minecraft.client.gui.screens.inventory.AbstractContainerScreen;
import net.minecraft.client.gui.components.Button;
import net.minecraft.client.Minecraft;

import net.mcreator.modscitymod.world.inventory.WallpaperGUIMenu;
import net.mcreator.modscitymod.network.WallpaperGUIButtonMessage;
import net.mcreator.modscitymod.ModsCityModMod;

import java.util.HashMap;

import com.mojang.blaze3d.vertex.PoseStack;
import com.mojang.blaze3d.systems.RenderSystem;

public class WallpaperGUIScreen extends AbstractContainerScreen<WallpaperGUIMenu> {
	private final static HashMap<String, Object> guistate = WallpaperGUIMenu.guistate;
	private final Level world;
	private final int x, y, z;
	private final Player entity;
	Button button_o;
	Button button_set;
	Button button_set1;
	Button button_set2;

	public WallpaperGUIScreen(WallpaperGUIMenu container, Inventory inventory, Component text) {
		super(container, inventory, text);
		this.world = container.world;
		this.x = container.x;
		this.y = container.y;
		this.z = container.z;
		this.entity = container.entity;
		this.imageWidth = 119;
		this.imageHeight = 191;
	}

	private static final ResourceLocation texture = new ResourceLocation("mods_city_mod:textures/screens/wallpaper_gui.png");

	@Override
	public void render(PoseStack ms, int mouseX, int mouseY, float partialTicks) {
		this.renderBackground(ms);
		super.render(ms, mouseX, mouseY, partialTicks);
		this.renderTooltip(ms, mouseX, mouseY);
	}

	@Override
	protected void renderBg(PoseStack ms, float partialTicks, int gx, int gy) {
		RenderSystem.setShaderColor(1, 1, 1, 1);
		RenderSystem.enableBlend();
		RenderSystem.defaultBlendFunc();
		RenderSystem.setShaderTexture(0, texture);
		this.blit(ms, this.leftPos, this.topPos, 0, 0, this.imageWidth, this.imageHeight, this.imageWidth, this.imageHeight);

		RenderSystem.setShaderTexture(0, new ResourceLocation("mods_city_mod:textures/screens/settingsicon.png"));
		this.blit(ms, this.leftPos + 14, this.topPos + 11, 0, 0, 14, 14, 14, 14);

		RenderSystem.setShaderTexture(0, new ResourceLocation("mods_city_mod:textures/screens/wall2preview.png"));
		this.blit(ms, this.leftPos + 23, this.topPos + 108, 0, 0, 21, 34, 21, 34);

		RenderSystem.setShaderTexture(0, new ResourceLocation("mods_city_mod:textures/screens/wall0preview.png"));
		this.blit(ms, this.leftPos + 23, this.topPos + 51, 0, 0, 21, 34, 21, 34);

		RenderSystem.setShaderTexture(0, new ResourceLocation("mods_city_mod:textures/screens/wall1preview.png"));
		this.blit(ms, this.leftPos + 77, this.topPos + 51, 0, 0, 21, 34, 21, 34);

		RenderSystem.setShaderTexture(0, new ResourceLocation("mods_city_mod:textures/screens/screentemplate.png"));
		this.blit(ms, this.leftPos + 0, this.topPos + 0, 0, 0, 119, 191, 119, 191);

		RenderSystem.disableBlend();
	}

	@Override
	public boolean keyPressed(int key, int b, int c) {
		if (key == 256) {
			this.minecraft.player.closeContainer();
			return true;
		}
		return super.keyPressed(key, b, c);
	}

	@Override
	public void containerTick() {
		super.containerTick();
	}

	@Override
	protected void renderLabels(PoseStack poseStack, int mouseX, int mouseY) {
		this.font.draw(poseStack, new TranslatableComponent("gui.mods_city_mod.wallpaper_gui.label_wallpaper"), 29, 13, -12829636);
		this.font.draw(poseStack, new TranslatableComponent("gui.mods_city_mod.wallpaper_gui.label_choose_a_wallpaper"), 14, 37, -12829636);
	}

	@Override
	public void onClose() {
		super.onClose();
		Minecraft.getInstance().keyboardHandler.setSendRepeatsToGui(false);
	}

	@Override
	public void init() {
		super.init();
		this.minecraft.keyboardHandler.setSendRepeatsToGui(true);
		button_o = new Button(this.leftPos + 41, this.topPos + 167, 35, 20, new TranslatableComponent("gui.mods_city_mod.wallpaper_gui.button_o"), e -> {
			if (true) {
				ModsCityModMod.PACKET_HANDLER.sendToServer(new WallpaperGUIButtonMessage(0, x, y, z));
				WallpaperGUIButtonMessage.handleButtonAction(entity, 0, x, y, z);
			}
		});
		guistate.put("button:button_o", button_o);
		this.addRenderableWidget(button_o);
		button_set = new Button(this.leftPos + 21, this.topPos + 87, 25, 20, new TranslatableComponent("gui.mods_city_mod.wallpaper_gui.button_set"), e -> {
			if (true) {
				ModsCityModMod.PACKET_HANDLER.sendToServer(new WallpaperGUIButtonMessage(1, x, y, z));
				WallpaperGUIButtonMessage.handleButtonAction(entity, 1, x, y, z);
			}
		});
		guistate.put("button:button_set", button_set);
		this.addRenderableWidget(button_set);
		button_set1 = new Button(this.leftPos + 75, this.topPos + 87, 25, 20, new TranslatableComponent("gui.mods_city_mod.wallpaper_gui.button_set1"), e -> {
			if (true) {
				ModsCityModMod.PACKET_HANDLER.sendToServer(new WallpaperGUIButtonMessage(2, x, y, z));
				WallpaperGUIButtonMessage.handleButtonAction(entity, 2, x, y, z);
			}
		});
		guistate.put("button:button_set1", button_set1);
		this.addRenderableWidget(button_set1);
		button_set2 = new Button(this.leftPos + 21, this.topPos + 144, 25, 20, new TranslatableComponent("gui.mods_city_mod.wallpaper_gui.button_set2"), e -> {
			if (true) {
				ModsCityModMod.PACKET_HANDLER.sendToServer(new WallpaperGUIButtonMessage(3, x, y, z));
				WallpaperGUIButtonMessage.handleButtonAction(entity, 3, x, y, z);
			}
		});
		guistate.put("button:button_set2", button_set2);
		this.addRenderableWidget(button_set2);
	}
}
