
package net.mcreator.modscitymod.client.gui;

import net.minecraft.world.level.Level;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.entity.player.Inventory;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.network.chat.TranslatableComponent;
import net.minecraft.network.chat.Component;
import net.minecraft.client.gui.screens.inventory.AbstractContainerScreen;
import net.minecraft.client.gui.components.EditBox;
import net.minecraft.client.gui.components.Button;
import net.minecraft.client.Minecraft;

import net.mcreator.modscitymod.world.inventory.WithdrawMenu;
import net.mcreator.modscitymod.network.WithdrawButtonMessage;
import net.mcreator.modscitymod.ModsCityModMod;

import java.util.HashMap;

import com.mojang.blaze3d.vertex.PoseStack;
import com.mojang.blaze3d.systems.RenderSystem;

public class WithdrawScreen extends AbstractContainerScreen<WithdrawMenu> {
	private final static HashMap<String, Object> guistate = WithdrawMenu.guistate;
	private final Level world;
	private final int x, y, z;
	private final Player entity;
	EditBox bucks;
	EditBox cents;
	EditBox balance;
	Button button_empty;
	Button button_empty1;
	Button button_empty2;
	Button button_empty3;
	Button button_withdraw;

	public WithdrawScreen(WithdrawMenu container, Inventory inventory, Component text) {
		super(container, inventory, text);
		this.world = container.world;
		this.x = container.x;
		this.y = container.y;
		this.z = container.z;
		this.entity = container.entity;
		this.imageWidth = 182;
		this.imageHeight = 199;
	}

	private static final ResourceLocation texture = new ResourceLocation("mods_city_mod:textures/screens/withdraw.png");

	@Override
	public void render(PoseStack ms, int mouseX, int mouseY, float partialTicks) {
		this.renderBackground(ms);
		super.render(ms, mouseX, mouseY, partialTicks);
		this.renderTooltip(ms, mouseX, mouseY);
		bucks.render(ms, mouseX, mouseY, partialTicks);
		cents.render(ms, mouseX, mouseY, partialTicks);
		balance.render(ms, mouseX, mouseY, partialTicks);
	}

	@Override
	protected void renderBg(PoseStack ms, float partialTicks, int gx, int gy) {
		RenderSystem.setShaderColor(1, 1, 1, 1);
		RenderSystem.enableBlend();
		RenderSystem.defaultBlendFunc();
		RenderSystem.setShaderTexture(0, texture);
		this.blit(ms, this.leftPos, this.topPos, 0, 0, this.imageWidth, this.imageHeight, this.imageWidth, this.imageHeight);
		RenderSystem.disableBlend();
	}

	@Override
	public boolean keyPressed(int key, int b, int c) {
		if (key == 256) {
			this.minecraft.player.closeContainer();
			return true;
		}
		if (bucks.isFocused())
			return bucks.keyPressed(key, b, c);
		if (cents.isFocused())
			return cents.keyPressed(key, b, c);
		if (balance.isFocused())
			return balance.keyPressed(key, b, c);
		return super.keyPressed(key, b, c);
	}

	@Override
	public void containerTick() {
		super.containerTick();
		bucks.tick();
		cents.tick();
		balance.tick();
	}

	@Override
	protected void renderLabels(PoseStack poseStack, int mouseX, int mouseY) {
		this.font.draw(poseStack, new TranslatableComponent("gui.mods_city_mod.withdraw.label_balance"), 9, 78, -12829636);
	}

	@Override
	public void onClose() {
		super.onClose();
		Minecraft.getInstance().keyboardHandler.setSendRepeatsToGui(false);
	}

	@Override
	public void init() {
		super.init();
		this.minecraft.keyboardHandler.setSendRepeatsToGui(true);
		bucks = new EditBox(this.font, this.leftPos + 30, this.topPos + 14, 120, 20, new TranslatableComponent("gui.mods_city_mod.withdraw.bucks")) {
			{
				setSuggestion(new TranslatableComponent("gui.mods_city_mod.withdraw.bucks").getString());
			}

			@Override
			public void insertText(String text) {
				super.insertText(text);
				if (getValue().isEmpty())
					setSuggestion(new TranslatableComponent("gui.mods_city_mod.withdraw.bucks").getString());
				else
					setSuggestion(null);
			}

			@Override
			public void moveCursorTo(int pos) {
				super.moveCursorTo(pos);
				if (getValue().isEmpty())
					setSuggestion(new TranslatableComponent("gui.mods_city_mod.withdraw.bucks").getString());
				else
					setSuggestion(null);
			}
		};
		bucks.setMaxLength(32767);
		guistate.put("text:bucks", bucks);
		this.addWidget(this.bucks);
		cents = new EditBox(this.font, this.leftPos + 30, this.topPos + 50, 120, 20, new TranslatableComponent("gui.mods_city_mod.withdraw.cents")) {
			{
				setSuggestion(new TranslatableComponent("gui.mods_city_mod.withdraw.cents").getString());
			}

			@Override
			public void insertText(String text) {
				super.insertText(text);
				if (getValue().isEmpty())
					setSuggestion(new TranslatableComponent("gui.mods_city_mod.withdraw.cents").getString());
				else
					setSuggestion(null);
			}

			@Override
			public void moveCursorTo(int pos) {
				super.moveCursorTo(pos);
				if (getValue().isEmpty())
					setSuggestion(new TranslatableComponent("gui.mods_city_mod.withdraw.cents").getString());
				else
					setSuggestion(null);
			}
		};
		cents.setMaxLength(32767);
		guistate.put("text:cents", cents);
		this.addWidget(this.cents);
		balance = new EditBox(this.font, this.leftPos + 53, this.topPos + 73, 120, 20, new TranslatableComponent("gui.mods_city_mod.withdraw.balance"));
		balance.setMaxLength(32767);
		guistate.put("text:balance", balance);
		this.addWidget(this.balance);
		button_empty = new Button(this.leftPos + 2, this.topPos + 14, 30, 20, new TranslatableComponent("gui.mods_city_mod.withdraw.button_empty"), e -> {
			if (true) {
				ModsCityModMod.PACKET_HANDLER.sendToServer(new WithdrawButtonMessage(0, x, y, z));
				WithdrawButtonMessage.handleButtonAction(entity, 0, x, y, z);
			}
		});
		guistate.put("button:button_empty", button_empty);
		this.addRenderableWidget(button_empty);
		button_empty1 = new Button(this.leftPos + 148, this.topPos + 14, 30, 20, new TranslatableComponent("gui.mods_city_mod.withdraw.button_empty1"), e -> {
			if (true) {
				ModsCityModMod.PACKET_HANDLER.sendToServer(new WithdrawButtonMessage(1, x, y, z));
				WithdrawButtonMessage.handleButtonAction(entity, 1, x, y, z);
			}
		});
		guistate.put("button:button_empty1", button_empty1);
		this.addRenderableWidget(button_empty1);
		button_empty2 = new Button(this.leftPos + 2, this.topPos + 50, 30, 20, new TranslatableComponent("gui.mods_city_mod.withdraw.button_empty2"), e -> {
			if (true) {
				ModsCityModMod.PACKET_HANDLER.sendToServer(new WithdrawButtonMessage(2, x, y, z));
				WithdrawButtonMessage.handleButtonAction(entity, 2, x, y, z);
			}
		});
		guistate.put("button:button_empty2", button_empty2);
		this.addRenderableWidget(button_empty2);
		button_empty3 = new Button(this.leftPos + 148, this.topPos + 50, 30, 20, new TranslatableComponent("gui.mods_city_mod.withdraw.button_empty3"), e -> {
			if (true) {
				ModsCityModMod.PACKET_HANDLER.sendToServer(new WithdrawButtonMessage(3, x, y, z));
				WithdrawButtonMessage.handleButtonAction(entity, 3, x, y, z);
			}
		});
		guistate.put("button:button_empty3", button_empty3);
		this.addRenderableWidget(button_empty3);
		button_withdraw = new Button(this.leftPos + 108, this.topPos + 95, 67, 20, new TranslatableComponent("gui.mods_city_mod.withdraw.button_withdraw"), e -> {
			if (true) {
				ModsCityModMod.PACKET_HANDLER.sendToServer(new WithdrawButtonMessage(4, x, y, z));
				WithdrawButtonMessage.handleButtonAction(entity, 4, x, y, z);
			}
		});
		guistate.put("button:button_withdraw", button_withdraw);
		this.addRenderableWidget(button_withdraw);
	}
}
