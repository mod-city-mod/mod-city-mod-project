
package net.mcreator.modscitymod.client.gui;

import net.minecraft.world.level.Level;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.entity.player.Inventory;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.network.chat.TranslatableComponent;
import net.minecraft.network.chat.Component;
import net.minecraft.client.gui.screens.inventory.AbstractContainerScreen;
import net.minecraft.client.gui.components.EditBox;
import net.minecraft.client.gui.components.Button;
import net.minecraft.client.Minecraft;

import net.mcreator.modscitymod.world.inventory.CustomerGUIMenu;
import net.mcreator.modscitymod.network.CustomerGUIButtonMessage;
import net.mcreator.modscitymod.ModsCityModMod;

import java.util.HashMap;

import com.mojang.blaze3d.vertex.PoseStack;
import com.mojang.blaze3d.systems.RenderSystem;

public class CustomerGUIScreen extends AbstractContainerScreen<CustomerGUIMenu> {
	private final static HashMap<String, Object> guistate = CustomerGUIMenu.guistate;
	private final Level world;
	private final int x, y, z;
	private final Player entity;
	EditBox payment;
	EditBox balance;
	Button button_pay;

	public CustomerGUIScreen(CustomerGUIMenu container, Inventory inventory, Component text) {
		super(container, inventory, text);
		this.world = container.world;
		this.x = container.x;
		this.y = container.y;
		this.z = container.z;
		this.entity = container.entity;
		this.imageWidth = 176;
		this.imageHeight = 204;
	}

	private static final ResourceLocation texture = new ResourceLocation("mods_city_mod:textures/screens/customer_gui.png");

	@Override
	public void render(PoseStack ms, int mouseX, int mouseY, float partialTicks) {
		this.renderBackground(ms);
		super.render(ms, mouseX, mouseY, partialTicks);
		this.renderTooltip(ms, mouseX, mouseY);
		payment.render(ms, mouseX, mouseY, partialTicks);
		balance.render(ms, mouseX, mouseY, partialTicks);
	}

	@Override
	protected void renderBg(PoseStack ms, float partialTicks, int gx, int gy) {
		RenderSystem.setShaderColor(1, 1, 1, 1);
		RenderSystem.enableBlend();
		RenderSystem.defaultBlendFunc();
		RenderSystem.setShaderTexture(0, texture);
		this.blit(ms, this.leftPos, this.topPos, 0, 0, this.imageWidth, this.imageHeight, this.imageWidth, this.imageHeight);
		RenderSystem.disableBlend();
	}

	@Override
	public boolean keyPressed(int key, int b, int c) {
		if (key == 256) {
			this.minecraft.player.closeContainer();
			return true;
		}
		if (payment.isFocused())
			return payment.keyPressed(key, b, c);
		if (balance.isFocused())
			return balance.keyPressed(key, b, c);
		return super.keyPressed(key, b, c);
	}

	@Override
	public void containerTick() {
		super.containerTick();
		payment.tick();
		balance.tick();
	}

	@Override
	protected void renderLabels(PoseStack poseStack, int mouseX, int mouseY) {
		this.font.draw(poseStack, new TranslatableComponent("gui.mods_city_mod.customer_gui.label_pay_with_card"), 6, 44, -12829636);
		this.font.draw(poseStack, new TranslatableComponent("gui.mods_city_mod.customer_gui.label_payment"), 6, 8, -12829636);
		this.font.draw(poseStack, new TranslatableComponent("gui.mods_city_mod.customer_gui.label_hold_it_in_your_hand"), 6, 53, -12829636);
		this.font.draw(poseStack, new TranslatableComponent("gui.mods_city_mod.customer_gui.label_balance"), 6, 98, -12829636);
	}

	@Override
	public void onClose() {
		super.onClose();
		Minecraft.getInstance().keyboardHandler.setSendRepeatsToGui(false);
	}

	@Override
	public void init() {
		super.init();
		this.minecraft.keyboardHandler.setSendRepeatsToGui(true);
		payment = new EditBox(this.font, this.leftPos + 51, this.topPos + 8, 120, 20, new TranslatableComponent("gui.mods_city_mod.customer_gui.payment")) {
			{
				setSuggestion(new TranslatableComponent("gui.mods_city_mod.customer_gui.payment").getString());
			}

			@Override
			public void insertText(String text) {
				super.insertText(text);
				if (getValue().isEmpty())
					setSuggestion(new TranslatableComponent("gui.mods_city_mod.customer_gui.payment").getString());
				else
					setSuggestion(null);
			}

			@Override
			public void moveCursorTo(int pos) {
				super.moveCursorTo(pos);
				if (getValue().isEmpty())
					setSuggestion(new TranslatableComponent("gui.mods_city_mod.customer_gui.payment").getString());
				else
					setSuggestion(null);
			}
		};
		payment.setMaxLength(32767);
		guistate.put("text:payment", payment);
		this.addWidget(this.payment);
		balance = new EditBox(this.font, this.leftPos + 51, this.topPos + 98, 120, 20, new TranslatableComponent("gui.mods_city_mod.customer_gui.balance"));
		balance.setMaxLength(32767);
		guistate.put("text:balance", balance);
		this.addWidget(this.balance);
		button_pay = new Button(this.leftPos + 6, this.topPos + 71, 40, 20, new TranslatableComponent("gui.mods_city_mod.customer_gui.button_pay"), e -> {
			if (true) {
				ModsCityModMod.PACKET_HANDLER.sendToServer(new CustomerGUIButtonMessage(0, x, y, z));
				CustomerGUIButtonMessage.handleButtonAction(entity, 0, x, y, z);
			}
		});
		guistate.put("button:button_pay", button_pay);
		this.addRenderableWidget(button_pay);
	}
}
