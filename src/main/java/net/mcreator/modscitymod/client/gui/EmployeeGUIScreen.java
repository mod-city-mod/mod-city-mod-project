
package net.mcreator.modscitymod.client.gui;

import net.minecraft.world.level.Level;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.entity.player.Inventory;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.network.chat.TranslatableComponent;
import net.minecraft.network.chat.Component;
import net.minecraft.client.gui.screens.inventory.AbstractContainerScreen;
import net.minecraft.client.gui.components.EditBox;
import net.minecraft.client.gui.components.Button;
import net.minecraft.client.Minecraft;

import net.mcreator.modscitymod.world.inventory.EmployeeGUIMenu;
import net.mcreator.modscitymod.network.EmployeeGUIButtonMessage;
import net.mcreator.modscitymod.ModsCityModMod;

import java.util.HashMap;

import com.mojang.blaze3d.vertex.PoseStack;
import com.mojang.blaze3d.systems.RenderSystem;

public class EmployeeGUIScreen extends AbstractContainerScreen<EmployeeGUIMenu> {
	private final static HashMap<String, Object> guistate = EmployeeGUIMenu.guistate;
	private final Level world;
	private final int x, y, z;
	private final Player entity;
	EditBox calc1;
	EditBox calc2;
	EditBox result;
	Button button_empty;
	Button button_empty1;
	Button button_empty2;
	Button button_empty3;
	Button button_pay;

	public EmployeeGUIScreen(EmployeeGUIMenu container, Inventory inventory, Component text) {
		super(container, inventory, text);
		this.world = container.world;
		this.x = container.x;
		this.y = container.y;
		this.z = container.z;
		this.entity = container.entity;
		this.imageWidth = 176;
		this.imageHeight = 207;
	}

	private static final ResourceLocation texture = new ResourceLocation("mods_city_mod:textures/screens/employee_gui.png");

	@Override
	public void render(PoseStack ms, int mouseX, int mouseY, float partialTicks) {
		this.renderBackground(ms);
		super.render(ms, mouseX, mouseY, partialTicks);
		this.renderTooltip(ms, mouseX, mouseY);
		calc1.render(ms, mouseX, mouseY, partialTicks);
		calc2.render(ms, mouseX, mouseY, partialTicks);
		result.render(ms, mouseX, mouseY, partialTicks);
	}

	@Override
	protected void renderBg(PoseStack ms, float partialTicks, int gx, int gy) {
		RenderSystem.setShaderColor(1, 1, 1, 1);
		RenderSystem.enableBlend();
		RenderSystem.defaultBlendFunc();
		RenderSystem.setShaderTexture(0, texture);
		this.blit(ms, this.leftPos, this.topPos, 0, 0, this.imageWidth, this.imageHeight, this.imageWidth, this.imageHeight);
		RenderSystem.disableBlend();
	}

	@Override
	public boolean keyPressed(int key, int b, int c) {
		if (key == 256) {
			this.minecraft.player.closeContainer();
			return true;
		}
		if (calc1.isFocused())
			return calc1.keyPressed(key, b, c);
		if (calc2.isFocused())
			return calc2.keyPressed(key, b, c);
		if (result.isFocused())
			return result.keyPressed(key, b, c);
		return super.keyPressed(key, b, c);
	}

	@Override
	public void containerTick() {
		super.containerTick();
		calc1.tick();
		calc2.tick();
		result.tick();
	}

	@Override
	protected void renderLabels(PoseStack poseStack, int mouseX, int mouseY) {
		this.font.draw(poseStack, new TranslatableComponent("gui.mods_city_mod.employee_gui.label_empty"), 6, 63, -12829636);
	}

	@Override
	public void onClose() {
		super.onClose();
		Minecraft.getInstance().keyboardHandler.setSendRepeatsToGui(false);
	}

	@Override
	public void init() {
		super.init();
		this.minecraft.keyboardHandler.setSendRepeatsToGui(true);
		calc1 = new EditBox(this.font, this.leftPos + 6, this.topPos + 9, 120, 20, new TranslatableComponent("gui.mods_city_mod.employee_gui.calc1"));
		calc1.setMaxLength(32767);
		guistate.put("text:calc1", calc1);
		this.addWidget(this.calc1);
		calc2 = new EditBox(this.font, this.leftPos + 6, this.topPos + 36, 120, 20, new TranslatableComponent("gui.mods_city_mod.employee_gui.calc2"));
		calc2.setMaxLength(32767);
		guistate.put("text:calc2", calc2);
		this.addWidget(this.calc2);
		result = new EditBox(this.font, this.leftPos + 6, this.topPos + 72, 120, 20, new TranslatableComponent("gui.mods_city_mod.employee_gui.result"));
		result.setMaxLength(32767);
		guistate.put("text:result", result);
		this.addWidget(this.result);
		button_empty = new Button(this.leftPos + 132, this.topPos + 9, 30, 20, new TranslatableComponent("gui.mods_city_mod.employee_gui.button_empty"), e -> {
			if (true) {
				ModsCityModMod.PACKET_HANDLER.sendToServer(new EmployeeGUIButtonMessage(0, x, y, z));
				EmployeeGUIButtonMessage.handleButtonAction(entity, 0, x, y, z);
			}
		});
		guistate.put("button:button_empty", button_empty);
		this.addRenderableWidget(button_empty);
		button_empty1 = new Button(this.leftPos + 132, this.topPos + 36, 30, 20, new TranslatableComponent("gui.mods_city_mod.employee_gui.button_empty1"), e -> {
			if (true) {
				ModsCityModMod.PACKET_HANDLER.sendToServer(new EmployeeGUIButtonMessage(1, x, y, z));
				EmployeeGUIButtonMessage.handleButtonAction(entity, 1, x, y, z);
			}
		});
		guistate.put("button:button_empty1", button_empty1);
		this.addRenderableWidget(button_empty1);
		button_empty2 = new Button(this.leftPos + 132, this.topPos + 63, 30, 20, new TranslatableComponent("gui.mods_city_mod.employee_gui.button_empty2"), e -> {
			if (true) {
				ModsCityModMod.PACKET_HANDLER.sendToServer(new EmployeeGUIButtonMessage(2, x, y, z));
				EmployeeGUIButtonMessage.handleButtonAction(entity, 2, x, y, z);
			}
		});
		guistate.put("button:button_empty2", button_empty2);
		this.addRenderableWidget(button_empty2);
		button_empty3 = new Button(this.leftPos + 132, this.topPos + 90, 30, 20, new TranslatableComponent("gui.mods_city_mod.employee_gui.button_empty3"), e -> {
			if (true) {
				ModsCityModMod.PACKET_HANDLER.sendToServer(new EmployeeGUIButtonMessage(3, x, y, z));
				EmployeeGUIButtonMessage.handleButtonAction(entity, 3, x, y, z);
			}
		});
		guistate.put("button:button_empty3", button_empty3);
		this.addRenderableWidget(button_empty3);
		button_pay = new Button(this.leftPos + 87, this.topPos + 99, 40, 20, new TranslatableComponent("gui.mods_city_mod.employee_gui.button_pay"), e -> {
			if (true) {
				ModsCityModMod.PACKET_HANDLER.sendToServer(new EmployeeGUIButtonMessage(4, x, y, z));
				EmployeeGUIButtonMessage.handleButtonAction(entity, 4, x, y, z);
			}
		});
		guistate.put("button:button_pay", button_pay);
		this.addRenderableWidget(button_pay);
	}
}
