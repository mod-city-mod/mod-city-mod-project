
package net.mcreator.modscitymod.client.gui;

import net.minecraft.world.level.Level;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.entity.player.Inventory;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.network.chat.TranslatableComponent;
import net.minecraft.network.chat.Component;
import net.minecraft.client.gui.screens.inventory.AbstractContainerScreen;
import net.minecraft.client.gui.components.EditBox;
import net.minecraft.client.gui.components.Button;
import net.minecraft.client.Minecraft;

import net.mcreator.modscitymod.world.inventory.MsgAppMenu;
import net.mcreator.modscitymod.network.MsgAppButtonMessage;
import net.mcreator.modscitymod.ModsCityModMod;

import java.util.HashMap;

import com.mojang.blaze3d.vertex.PoseStack;
import com.mojang.blaze3d.systems.RenderSystem;

public class MsgAppScreen extends AbstractContainerScreen<MsgAppMenu> {
	private final static HashMap<String, Object> guistate = MsgAppMenu.guistate;
	private final Level world;
	private final int x, y, z;
	private final Player entity;
	EditBox MsgInsert;
	EditBox msg;
	Button button_send;
	Button button_refresh;
	Button button_x;

	public MsgAppScreen(MsgAppMenu container, Inventory inventory, Component text) {
		super(container, inventory, text);
		this.world = container.world;
		this.x = container.x;
		this.y = container.y;
		this.z = container.z;
		this.entity = container.entity;
		this.imageWidth = 175;
		this.imageHeight = 131;
	}

	private static final ResourceLocation texture = new ResourceLocation("mods_city_mod:textures/screens/msg_app.png");

	@Override
	public void render(PoseStack ms, int mouseX, int mouseY, float partialTicks) {
		this.renderBackground(ms);
		super.render(ms, mouseX, mouseY, partialTicks);
		this.renderTooltip(ms, mouseX, mouseY);
		MsgInsert.render(ms, mouseX, mouseY, partialTicks);
		msg.render(ms, mouseX, mouseY, partialTicks);
	}

	@Override
	protected void renderBg(PoseStack ms, float partialTicks, int gx, int gy) {
		RenderSystem.setShaderColor(1, 1, 1, 1);
		RenderSystem.enableBlend();
		RenderSystem.defaultBlendFunc();
		RenderSystem.setShaderTexture(0, texture);
		this.blit(ms, this.leftPos, this.topPos, 0, 0, this.imageWidth, this.imageHeight, this.imageWidth, this.imageHeight);

		RenderSystem.setShaderTexture(0, new ResourceLocation("mods_city_mod:textures/screens/messages.png"));
		this.blit(ms, this.leftPos + 4, this.topPos + 4, 0, 0, 12, 14, 12, 14);

		RenderSystem.setShaderTexture(0, new ResourceLocation("mods_city_mod:textures/screens/msgtail.png"));
		this.blit(ms, this.leftPos + 160, this.topPos + 48, 0, 0, 6, 6, 6, 6);

		RenderSystem.disableBlend();
	}

	@Override
	public boolean keyPressed(int key, int b, int c) {
		if (key == 256) {
			this.minecraft.player.closeContainer();
			return true;
		}
		if (MsgInsert.isFocused())
			return MsgInsert.keyPressed(key, b, c);
		if (msg.isFocused())
			return msg.keyPressed(key, b, c);
		return super.keyPressed(key, b, c);
	}

	@Override
	public void containerTick() {
		super.containerTick();
		MsgInsert.tick();
		msg.tick();
	}

	@Override
	protected void renderLabels(PoseStack poseStack, int mouseX, int mouseY) {
		this.font.draw(poseStack, new TranslatableComponent("gui.mods_city_mod.msg_app.label_messages"), 18, 5, -12829636);
		this.font.draw(poseStack, new TranslatableComponent("gui.mods_city_mod.msg_app.label_not_working"), 6, 67, -12829636);
	}

	@Override
	public void onClose() {
		super.onClose();
		Minecraft.getInstance().keyboardHandler.setSendRepeatsToGui(false);
	}

	@Override
	public void init() {
		super.init();
		this.minecraft.keyboardHandler.setSendRepeatsToGui(true);
		MsgInsert = new EditBox(this.font, this.leftPos + 8, this.topPos + 106, 120, 20, new TranslatableComponent("gui.mods_city_mod.msg_app.MsgInsert")) {
			{
				setSuggestion(new TranslatableComponent("gui.mods_city_mod.msg_app.MsgInsert").getString());
			}

			@Override
			public void insertText(String text) {
				super.insertText(text);
				if (getValue().isEmpty())
					setSuggestion(new TranslatableComponent("gui.mods_city_mod.msg_app.MsgInsert").getString());
				else
					setSuggestion(null);
			}

			@Override
			public void moveCursorTo(int pos) {
				super.moveCursorTo(pos);
				if (getValue().isEmpty())
					setSuggestion(new TranslatableComponent("gui.mods_city_mod.msg_app.MsgInsert").getString());
				else
					setSuggestion(null);
			}
		};
		MsgInsert.setMaxLength(32767);
		guistate.put("text:MsgInsert", MsgInsert);
		this.addWidget(this.MsgInsert);
		msg = new EditBox(this.font, this.leftPos + 46, this.topPos + 28, 120, 20, new TranslatableComponent("gui.mods_city_mod.msg_app.msg")) {
			{
				setSuggestion(new TranslatableComponent("gui.mods_city_mod.msg_app.msg").getString());
			}

			@Override
			public void insertText(String text) {
				super.insertText(text);
				if (getValue().isEmpty())
					setSuggestion(new TranslatableComponent("gui.mods_city_mod.msg_app.msg").getString());
				else
					setSuggestion(null);
			}

			@Override
			public void moveCursorTo(int pos) {
				super.moveCursorTo(pos);
				if (getValue().isEmpty())
					setSuggestion(new TranslatableComponent("gui.mods_city_mod.msg_app.msg").getString());
				else
					setSuggestion(null);
			}
		};
		msg.setMaxLength(32767);
		guistate.put("text:msg", msg);
		this.addWidget(this.msg);
		button_send = new Button(this.leftPos + 128, this.topPos + 106, 41, 20, new TranslatableComponent("gui.mods_city_mod.msg_app.button_send"), e -> {
			if (true) {
				ModsCityModMod.PACKET_HANDLER.sendToServer(new MsgAppButtonMessage(0, x, y, z));
				MsgAppButtonMessage.handleButtonAction(entity, 0, x, y, z);
			}
		});
		guistate.put("button:button_send", button_send);
		this.addRenderableWidget(button_send);
		button_refresh = new Button(this.leftPos + 6, this.topPos + 81, 45, 20, new TranslatableComponent("gui.mods_city_mod.msg_app.button_refresh"), e -> {
			if (true) {
				ModsCityModMod.PACKET_HANDLER.sendToServer(new MsgAppButtonMessage(1, x, y, z));
				MsgAppButtonMessage.handleButtonAction(entity, 1, x, y, z);
			}
		});
		guistate.put("button:button_refresh", button_refresh);
		this.addRenderableWidget(button_refresh);
		button_x = new Button(this.leftPos + 153, this.topPos + 3, 19, 20, new TranslatableComponent("gui.mods_city_mod.msg_app.button_x"), e -> {
			if (true) {
				ModsCityModMod.PACKET_HANDLER.sendToServer(new MsgAppButtonMessage(2, x, y, z));
				MsgAppButtonMessage.handleButtonAction(entity, 2, x, y, z);
			}
		});
		guistate.put("button:button_x", button_x);
		this.addRenderableWidget(button_x);
	}
}
