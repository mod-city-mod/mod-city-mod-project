
package net.mcreator.modscitymod.client.gui;

import net.minecraft.world.level.Level;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.entity.player.Inventory;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.network.chat.TranslatableComponent;
import net.minecraft.network.chat.Component;
import net.minecraft.client.gui.screens.inventory.AbstractContainerScreen;
import net.minecraft.client.gui.components.Button;
import net.minecraft.client.Minecraft;

import net.mcreator.modscitymod.world.inventory.SettingsGUIMenu;
import net.mcreator.modscitymod.network.SettingsGUIButtonMessage;
import net.mcreator.modscitymod.ModsCityModMod;

import java.util.HashMap;

import com.mojang.blaze3d.vertex.PoseStack;
import com.mojang.blaze3d.systems.RenderSystem;

public class SettingsGUIScreen extends AbstractContainerScreen<SettingsGUIMenu> {
	private final static HashMap<String, Object> guistate = SettingsGUIMenu.guistate;
	private final Level world;
	private final int x, y, z;
	private final Player entity;
	Button button_o;
	Button button_wallpaper;

	public SettingsGUIScreen(SettingsGUIMenu container, Inventory inventory, Component text) {
		super(container, inventory, text);
		this.world = container.world;
		this.x = container.x;
		this.y = container.y;
		this.z = container.z;
		this.entity = container.entity;
		this.imageWidth = 119;
		this.imageHeight = 191;
	}

	private static final ResourceLocation texture = new ResourceLocation("mods_city_mod:textures/screens/settings_gui.png");

	@Override
	public void render(PoseStack ms, int mouseX, int mouseY, float partialTicks) {
		this.renderBackground(ms);
		super.render(ms, mouseX, mouseY, partialTicks);
		this.renderTooltip(ms, mouseX, mouseY);
	}

	@Override
	protected void renderBg(PoseStack ms, float partialTicks, int gx, int gy) {
		RenderSystem.setShaderColor(1, 1, 1, 1);
		RenderSystem.enableBlend();
		RenderSystem.defaultBlendFunc();
		RenderSystem.setShaderTexture(0, texture);
		this.blit(ms, this.leftPos, this.topPos, 0, 0, this.imageWidth, this.imageHeight, this.imageWidth, this.imageHeight);

		RenderSystem.setShaderTexture(0, new ResourceLocation("mods_city_mod:textures/screens/settingsicon.png"));
		this.blit(ms, this.leftPos + 14, this.topPos + 11, 0, 0, 14, 14, 14, 14);

		RenderSystem.setShaderTexture(0, new ResourceLocation("mods_city_mod:textures/screens/screentemplate.png"));
		this.blit(ms, this.leftPos + 0, this.topPos + 0, 0, 0, 119, 191, 119, 191);

		RenderSystem.disableBlend();
	}

	@Override
	public boolean keyPressed(int key, int b, int c) {
		if (key == 256) {
			this.minecraft.player.closeContainer();
			return true;
		}
		return super.keyPressed(key, b, c);
	}

	@Override
	public void containerTick() {
		super.containerTick();
	}

	@Override
	protected void renderLabels(PoseStack poseStack, int mouseX, int mouseY) {
		this.font.draw(poseStack, new TranslatableComponent("gui.mods_city_mod.settings_gui.label_settings"), 30, 13, -12829636);
		this.font.draw(poseStack, new TranslatableComponent("gui.mods_city_mod.settings_gui.label_customization"), 14, 34, -12829636);
	}

	@Override
	public void onClose() {
		super.onClose();
		Minecraft.getInstance().keyboardHandler.setSendRepeatsToGui(false);
	}

	@Override
	public void init() {
		super.init();
		this.minecraft.keyboardHandler.setSendRepeatsToGui(true);
		button_o = new Button(this.leftPos + 41, this.topPos + 167, 35, 20, new TranslatableComponent("gui.mods_city_mod.settings_gui.button_o"), e -> {
			if (true) {
				ModsCityModMod.PACKET_HANDLER.sendToServer(new SettingsGUIButtonMessage(0, x, y, z));
				SettingsGUIButtonMessage.handleButtonAction(entity, 0, x, y, z);
			}
		});
		guistate.put("button:button_o", button_o);
		this.addRenderableWidget(button_o);
		button_wallpaper = new Button(this.leftPos + 23, this.topPos + 46, 72, 20, new TranslatableComponent("gui.mods_city_mod.settings_gui.button_wallpaper"), e -> {
			if (true) {
				ModsCityModMod.PACKET_HANDLER.sendToServer(new SettingsGUIButtonMessage(1, x, y, z));
				SettingsGUIButtonMessage.handleButtonAction(entity, 1, x, y, z);
			}
		});
		guistate.put("button:button_wallpaper", button_wallpaper);
		this.addRenderableWidget(button_wallpaper);
	}
}
