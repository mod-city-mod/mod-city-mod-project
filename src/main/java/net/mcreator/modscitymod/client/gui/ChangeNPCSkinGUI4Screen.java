
package net.mcreator.modscitymod.client.gui;

import net.minecraft.world.level.Level;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.entity.player.Inventory;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.network.chat.TranslatableComponent;
import net.minecraft.network.chat.Component;
import net.minecraft.client.gui.screens.inventory.AbstractContainerScreen;
import net.minecraft.client.gui.components.Button;
import net.minecraft.client.Minecraft;

import net.mcreator.modscitymod.world.inventory.ChangeNPCSkinGUI4Menu;
import net.mcreator.modscitymod.network.ChangeNPCSkinGUI4ButtonMessage;
import net.mcreator.modscitymod.ModsCityModMod;

import java.util.HashMap;

import com.mojang.blaze3d.vertex.PoseStack;
import com.mojang.blaze3d.systems.RenderSystem;

public class ChangeNPCSkinGUI4Screen extends AbstractContainerScreen<ChangeNPCSkinGUI4Menu> {
	private final static HashMap<String, Object> guistate = ChangeNPCSkinGUI4Menu.guistate;
	private final Level world;
	private final int x, y, z;
	private final Player entity;
	Button button_empty;
	Button button_empty1;
	Button button_set;

	public ChangeNPCSkinGUI4Screen(ChangeNPCSkinGUI4Menu container, Inventory inventory, Component text) {
		super(container, inventory, text);
		this.world = container.world;
		this.x = container.x;
		this.y = container.y;
		this.z = container.z;
		this.entity = container.entity;
		this.imageWidth = 176;
		this.imageHeight = 166;
	}

	private static final ResourceLocation texture = new ResourceLocation("mods_city_mod:textures/screens/change_npc_skin_gui_4.png");

	@Override
	public void render(PoseStack ms, int mouseX, int mouseY, float partialTicks) {
		this.renderBackground(ms);
		super.render(ms, mouseX, mouseY, partialTicks);
		this.renderTooltip(ms, mouseX, mouseY);
	}

	@Override
	protected void renderBg(PoseStack ms, float partialTicks, int gx, int gy) {
		RenderSystem.setShaderColor(1, 1, 1, 1);
		RenderSystem.enableBlend();
		RenderSystem.defaultBlendFunc();
		RenderSystem.setShaderTexture(0, texture);
		this.blit(ms, this.leftPos, this.topPos, 0, 0, this.imageWidth, this.imageHeight, this.imageWidth, this.imageHeight);

		RenderSystem.setShaderTexture(0, new ResourceLocation("mods_city_mod:textures/screens/makena.png"));
		this.blit(ms, this.leftPos + 72, this.topPos + 34, 0, 0, 32, 64, 32, 64);

		RenderSystem.disableBlend();
	}

	@Override
	public boolean keyPressed(int key, int b, int c) {
		if (key == 256) {
			this.minecraft.player.closeContainer();
			return true;
		}
		return super.keyPressed(key, b, c);
	}

	@Override
	public void containerTick() {
		super.containerTick();
	}

	@Override
	protected void renderLabels(PoseStack poseStack, int mouseX, int mouseY) {
		this.font.draw(poseStack, new TranslatableComponent("gui.mods_city_mod.change_npc_skin_gui_4.label_choose_skin"), 60, 7, -12829636);
		this.font.draw(poseStack, new TranslatableComponent("gui.mods_city_mod.change_npc_skin_gui_4.label_makena"), 72, 106, -12829636);
	}

	@Override
	public void onClose() {
		super.onClose();
		Minecraft.getInstance().keyboardHandler.setSendRepeatsToGui(false);
	}

	@Override
	public void init() {
		super.init();
		this.minecraft.keyboardHandler.setSendRepeatsToGui(true);
		button_empty = new Button(this.leftPos + 144, this.topPos + 61, 25, 20, new TranslatableComponent("gui.mods_city_mod.change_npc_skin_gui_4.button_empty"), e -> {
			if (true) {
				ModsCityModMod.PACKET_HANDLER.sendToServer(new ChangeNPCSkinGUI4ButtonMessage(0, x, y, z));
				ChangeNPCSkinGUI4ButtonMessage.handleButtonAction(entity, 0, x, y, z);
			}
		});
		guistate.put("button:button_empty", button_empty);
		this.addRenderableWidget(button_empty);
		button_empty1 = new Button(this.leftPos + 6, this.topPos + 61, 25, 20, new TranslatableComponent("gui.mods_city_mod.change_npc_skin_gui_4.button_empty1"), e -> {
			if (true) {
				ModsCityModMod.PACKET_HANDLER.sendToServer(new ChangeNPCSkinGUI4ButtonMessage(1, x, y, z));
				ChangeNPCSkinGUI4ButtonMessage.handleButtonAction(entity, 1, x, y, z);
			}
		});
		guistate.put("button:button_empty1", button_empty1);
		this.addRenderableWidget(button_empty1);
		button_set = new Button(this.leftPos + 66, this.topPos + 133, 40, 20, new TranslatableComponent("gui.mods_city_mod.change_npc_skin_gui_4.button_set"), e -> {
			if (true) {
				ModsCityModMod.PACKET_HANDLER.sendToServer(new ChangeNPCSkinGUI4ButtonMessage(2, x, y, z));
				ChangeNPCSkinGUI4ButtonMessage.handleButtonAction(entity, 2, x, y, z);
			}
		});
		guistate.put("button:button_set", button_set);
		this.addRenderableWidget(button_set);
	}
}
