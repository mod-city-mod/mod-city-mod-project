
package net.mcreator.modscitymod.client.renderer;

import net.minecraft.resources.ResourceLocation;
import net.minecraft.client.renderer.entity.MobRenderer;
import net.minecraft.client.renderer.entity.EntityRendererProvider;

import net.mcreator.modscitymod.entity.Npc1Entity;
import net.mcreator.modscitymod.client.model.Modelnpc;

public class Npc1Renderer extends MobRenderer<Npc1Entity, Modelnpc<Npc1Entity>> {
	public Npc1Renderer(EntityRendererProvider.Context context) {
		super(context, new Modelnpc(context.bakeLayer(Modelnpc.LAYER_LOCATION)), 0.5f);
	}

	@Override
	public ResourceLocation getTextureLocation(Npc1Entity entity) {
		return new ResourceLocation("mods_city_mod:textures/entities/ari.png");
	}
}
