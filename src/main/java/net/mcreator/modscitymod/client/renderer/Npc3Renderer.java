
package net.mcreator.modscitymod.client.renderer;

import net.minecraft.resources.ResourceLocation;
import net.minecraft.client.renderer.entity.MobRenderer;
import net.minecraft.client.renderer.entity.EntityRendererProvider;

import net.mcreator.modscitymod.entity.Npc3Entity;
import net.mcreator.modscitymod.client.model.Modelnpc;

public class Npc3Renderer extends MobRenderer<Npc3Entity, Modelnpc<Npc3Entity>> {
	public Npc3Renderer(EntityRendererProvider.Context context) {
		super(context, new Modelnpc(context.bakeLayer(Modelnpc.LAYER_LOCATION)), 0.5f);
	}

	@Override
	public ResourceLocation getTextureLocation(Npc3Entity entity) {
		return new ResourceLocation("mods_city_mod:textures/entities/kai.png");
	}
}
