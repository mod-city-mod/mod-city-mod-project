
package net.mcreator.modscitymod.client.renderer;

import net.minecraft.resources.ResourceLocation;
import net.minecraft.client.renderer.entity.MobRenderer;
import net.minecraft.client.renderer.entity.EntityRendererProvider;

import net.mcreator.modscitymod.entity.Npc8Entity;
import net.mcreator.modscitymod.client.model.Modelnpc;

public class Npc8Renderer extends MobRenderer<Npc8Entity, Modelnpc<Npc8Entity>> {
	public Npc8Renderer(EntityRendererProvider.Context context) {
		super(context, new Modelnpc(context.bakeLayer(Modelnpc.LAYER_LOCATION)), 0.5f);
	}

	@Override
	public ResourceLocation getTextureLocation(Npc8Entity entity) {
		return new ResourceLocation("mods_city_mod:textures/entities/zuri.png");
	}
}
