
package net.mcreator.modscitymod.client.renderer;

import net.minecraft.resources.ResourceLocation;
import net.minecraft.client.renderer.entity.MobRenderer;
import net.minecraft.client.renderer.entity.EntityRendererProvider;

import net.mcreator.modscitymod.entity.Npc5Entity;
import net.mcreator.modscitymod.client.model.Modelnpc;

public class Npc5Renderer extends MobRenderer<Npc5Entity, Modelnpc<Npc5Entity>> {
	public Npc5Renderer(EntityRendererProvider.Context context) {
		super(context, new Modelnpc(context.bakeLayer(Modelnpc.LAYER_LOCATION)), 0.5f);
	}

	@Override
	public ResourceLocation getTextureLocation(Npc5Entity entity) {
		return new ResourceLocation("mods_city_mod:textures/entities/noor.png");
	}
}
