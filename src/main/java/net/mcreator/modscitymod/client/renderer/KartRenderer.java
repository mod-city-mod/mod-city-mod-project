
package net.mcreator.modscitymod.client.renderer;

import net.minecraft.resources.ResourceLocation;
import net.minecraft.client.renderer.entity.MobRenderer;
import net.minecraft.client.renderer.entity.EntityRendererProvider;

import net.mcreator.modscitymod.entity.KartEntity;
import net.mcreator.modscitymod.client.model.Modelcustom_model;

public class KartRenderer extends MobRenderer<KartEntity, Modelcustom_model<KartEntity>> {
	public KartRenderer(EntityRendererProvider.Context context) {
		super(context, new Modelcustom_model(context.bakeLayer(Modelcustom_model.LAYER_LOCATION)), 0.5f);
	}

	@Override
	public ResourceLocation getTextureLocation(KartEntity entity) {
		return new ResourceLocation("mods_city_mod:textures/entities/kart.png");
	}
}
