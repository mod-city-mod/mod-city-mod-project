
package net.mcreator.modscitymod.client.renderer;

import net.minecraft.resources.ResourceLocation;
import net.minecraft.client.renderer.entity.MobRenderer;
import net.minecraft.client.renderer.entity.EntityRendererProvider;

import net.mcreator.modscitymod.entity.NpcEntity;
import net.mcreator.modscitymod.client.model.Modelnpc;

public class NpcRenderer extends MobRenderer<NpcEntity, Modelnpc<NpcEntity>> {
	public NpcRenderer(EntityRendererProvider.Context context) {
		super(context, new Modelnpc(context.bakeLayer(Modelnpc.LAYER_LOCATION)), 0.5f);
	}

	@Override
	public ResourceLocation getTextureLocation(NpcEntity entity) {
		return new ResourceLocation("mods_city_mod:textures/entities/alex.png");
	}
}
