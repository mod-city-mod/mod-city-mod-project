
package net.mcreator.modscitymod.client.renderer;

import net.minecraft.resources.ResourceLocation;
import net.minecraft.client.renderer.entity.MobRenderer;
import net.minecraft.client.renderer.entity.EntityRendererProvider;

import net.mcreator.modscitymod.entity.Npc4Entity;
import net.mcreator.modscitymod.client.model.Modelnpc;

public class Npc4Renderer extends MobRenderer<Npc4Entity, Modelnpc<Npc4Entity>> {
	public Npc4Renderer(EntityRendererProvider.Context context) {
		super(context, new Modelnpc(context.bakeLayer(Modelnpc.LAYER_LOCATION)), 0.5f);
	}

	@Override
	public ResourceLocation getTextureLocation(Npc4Entity entity) {
		return new ResourceLocation("mods_city_mod:textures/entities/makena.png");
	}
}
