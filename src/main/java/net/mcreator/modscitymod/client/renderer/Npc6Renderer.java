
package net.mcreator.modscitymod.client.renderer;

import net.minecraft.resources.ResourceLocation;
import net.minecraft.client.renderer.entity.MobRenderer;
import net.minecraft.client.renderer.entity.EntityRendererProvider;

import net.mcreator.modscitymod.entity.Npc6Entity;
import net.mcreator.modscitymod.client.model.Modelnpc;

public class Npc6Renderer extends MobRenderer<Npc6Entity, Modelnpc<Npc6Entity>> {
	public Npc6Renderer(EntityRendererProvider.Context context) {
		super(context, new Modelnpc(context.bakeLayer(Modelnpc.LAYER_LOCATION)), 0.5f);
	}

	@Override
	public ResourceLocation getTextureLocation(Npc6Entity entity) {
		return new ResourceLocation("mods_city_mod:textures/entities/steve.png");
	}
}
