
/*
 *    MCreator note: This file will be REGENERATED on each build.
 */
package net.mcreator.modscitymod.init;

import net.minecraftforge.registries.RegistryObject;
import net.minecraftforge.registries.ForgeRegistries;
import net.minecraftforge.registries.DeferredRegister;
import net.minecraftforge.fml.event.lifecycle.FMLCommonSetupEvent;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.event.entity.EntityAttributeCreationEvent;

import net.minecraft.world.entity.MobCategory;
import net.minecraft.world.entity.EntityType;
import net.minecraft.world.entity.Entity;

import net.mcreator.modscitymod.entity.NpcEntity;
import net.mcreator.modscitymod.entity.Npc8Entity;
import net.mcreator.modscitymod.entity.Npc7Entity;
import net.mcreator.modscitymod.entity.Npc6Entity;
import net.mcreator.modscitymod.entity.Npc5Entity;
import net.mcreator.modscitymod.entity.Npc4Entity;
import net.mcreator.modscitymod.entity.Npc3Entity;
import net.mcreator.modscitymod.entity.Npc2Entity;
import net.mcreator.modscitymod.entity.Npc1Entity;
import net.mcreator.modscitymod.entity.KartEntity;
import net.mcreator.modscitymod.ModsCityModMod;

@Mod.EventBusSubscriber(bus = Mod.EventBusSubscriber.Bus.MOD)
public class ModsCityModModEntities {
	public static final DeferredRegister<EntityType<?>> REGISTRY = DeferredRegister.create(ForgeRegistries.ENTITIES, ModsCityModMod.MODID);
	public static final RegistryObject<EntityType<NpcEntity>> NPC = register("npc",
			EntityType.Builder.<NpcEntity>of(NpcEntity::new, MobCategory.MONSTER).setShouldReceiveVelocityUpdates(true).setTrackingRange(64).setUpdateInterval(3).setCustomClientFactory(NpcEntity::new)

					.sized(0.6f, 1.8f));
	public static final RegistryObject<EntityType<KartEntity>> KART = register("kart",
			EntityType.Builder.<KartEntity>of(KartEntity::new, MobCategory.MONSTER).setShouldReceiveVelocityUpdates(true).setTrackingRange(64).setUpdateInterval(3).setCustomClientFactory(KartEntity::new)

					.sized(1f, 0.7f));
	public static final RegistryObject<EntityType<Npc6Entity>> NPC_6 = register("npc_6",
			EntityType.Builder.<Npc6Entity>of(Npc6Entity::new, MobCategory.MONSTER).setShouldReceiveVelocityUpdates(true).setTrackingRange(64).setUpdateInterval(3).setCustomClientFactory(Npc6Entity::new)

					.sized(0.6f, 1.8f));
	public static final RegistryObject<EntityType<Npc1Entity>> NPC_1 = register("npc_1",
			EntityType.Builder.<Npc1Entity>of(Npc1Entity::new, MobCategory.MONSTER).setShouldReceiveVelocityUpdates(true).setTrackingRange(64).setUpdateInterval(3).setCustomClientFactory(Npc1Entity::new)

					.sized(0.6f, 1.8f));
	public static final RegistryObject<EntityType<Npc2Entity>> NPC_2 = register("npc_2",
			EntityType.Builder.<Npc2Entity>of(Npc2Entity::new, MobCategory.MONSTER).setShouldReceiveVelocityUpdates(true).setTrackingRange(64).setUpdateInterval(3).setCustomClientFactory(Npc2Entity::new)

					.sized(0.6f, 1.8f));
	public static final RegistryObject<EntityType<Npc3Entity>> NPC_3 = register("npc_3",
			EntityType.Builder.<Npc3Entity>of(Npc3Entity::new, MobCategory.MONSTER).setShouldReceiveVelocityUpdates(true).setTrackingRange(64).setUpdateInterval(3).setCustomClientFactory(Npc3Entity::new)

					.sized(0.6f, 1.8f));
	public static final RegistryObject<EntityType<Npc4Entity>> NPC_4 = register("npc_4",
			EntityType.Builder.<Npc4Entity>of(Npc4Entity::new, MobCategory.MONSTER).setShouldReceiveVelocityUpdates(true).setTrackingRange(64).setUpdateInterval(3).setCustomClientFactory(Npc4Entity::new)

					.sized(0.6f, 1.8f));
	public static final RegistryObject<EntityType<Npc5Entity>> NPC_5 = register("npc_5",
			EntityType.Builder.<Npc5Entity>of(Npc5Entity::new, MobCategory.MONSTER).setShouldReceiveVelocityUpdates(true).setTrackingRange(64).setUpdateInterval(3).setCustomClientFactory(Npc5Entity::new)

					.sized(0.6f, 1.8f));
	public static final RegistryObject<EntityType<Npc7Entity>> NPC_7 = register("npc_7",
			EntityType.Builder.<Npc7Entity>of(Npc7Entity::new, MobCategory.MONSTER).setShouldReceiveVelocityUpdates(true).setTrackingRange(64).setUpdateInterval(3).setCustomClientFactory(Npc7Entity::new)

					.sized(0.6f, 1.8f));
	public static final RegistryObject<EntityType<Npc8Entity>> NPC_8 = register("npc_8",
			EntityType.Builder.<Npc8Entity>of(Npc8Entity::new, MobCategory.MONSTER).setShouldReceiveVelocityUpdates(true).setTrackingRange(64).setUpdateInterval(3).setCustomClientFactory(Npc8Entity::new)

					.sized(0.6f, 1.8f));

	private static <T extends Entity> RegistryObject<EntityType<T>> register(String registryname, EntityType.Builder<T> entityTypeBuilder) {
		return REGISTRY.register(registryname, () -> (EntityType<T>) entityTypeBuilder.build(registryname));
	}

	@SubscribeEvent
	public static void init(FMLCommonSetupEvent event) {
		event.enqueueWork(() -> {
			NpcEntity.init();
			KartEntity.init();
			Npc6Entity.init();
			Npc1Entity.init();
			Npc2Entity.init();
			Npc3Entity.init();
			Npc4Entity.init();
			Npc5Entity.init();
			Npc7Entity.init();
			Npc8Entity.init();
		});
	}

	@SubscribeEvent
	public static void registerAttributes(EntityAttributeCreationEvent event) {
		event.put(NPC.get(), NpcEntity.createAttributes().build());
		event.put(KART.get(), KartEntity.createAttributes().build());
		event.put(NPC_6.get(), Npc6Entity.createAttributes().build());
		event.put(NPC_1.get(), Npc1Entity.createAttributes().build());
		event.put(NPC_2.get(), Npc2Entity.createAttributes().build());
		event.put(NPC_3.get(), Npc3Entity.createAttributes().build());
		event.put(NPC_4.get(), Npc4Entity.createAttributes().build());
		event.put(NPC_5.get(), Npc5Entity.createAttributes().build());
		event.put(NPC_7.get(), Npc7Entity.createAttributes().build());
		event.put(NPC_8.get(), Npc8Entity.createAttributes().build());
	}
}
