
/*
 *    MCreator note: This file will be REGENERATED on each build.
 */
package net.mcreator.modscitymod.init;

import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.client.event.ParticleFactoryRegisterEvent;
import net.minecraftforge.api.distmarker.Dist;

import net.minecraft.core.particles.SimpleParticleType;
import net.minecraft.client.Minecraft;

import net.mcreator.modscitymod.client.particle.VehicleVoidDisplayParticle;
import net.mcreator.modscitymod.client.particle.VehicleCheckpointDisplayParticle;
import net.mcreator.modscitymod.client.particle.OffroadDisplayParticle;
import net.mcreator.modscitymod.client.particle.KartTrickDisplayParticle;

@Mod.EventBusSubscriber(bus = Mod.EventBusSubscriber.Bus.MOD, value = Dist.CLIENT)
public class ModsCityModModParticles {
	@SubscribeEvent
	public static void registerParticles(ParticleFactoryRegisterEvent event) {
		Minecraft.getInstance().particleEngine.register((SimpleParticleType) ModsCityModModParticleTypes.VEHICLE_VOID_DISPLAY.get(), VehicleVoidDisplayParticle::provider);
		Minecraft.getInstance().particleEngine.register((SimpleParticleType) ModsCityModModParticleTypes.OFFROAD_DISPLAY.get(), OffroadDisplayParticle::provider);
		Minecraft.getInstance().particleEngine.register((SimpleParticleType) ModsCityModModParticleTypes.KART_TRICK_DISPLAY.get(), KartTrickDisplayParticle::provider);
		Minecraft.getInstance().particleEngine.register((SimpleParticleType) ModsCityModModParticleTypes.VEHICLE_CHECKPOINT_DISPLAY.get(), VehicleCheckpointDisplayParticle::provider);
	}
}
