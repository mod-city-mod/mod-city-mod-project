
/*
 *    MCreator note: This file will be REGENERATED on each build.
 */
package net.mcreator.modscitymod.init;

import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.client.event.EntityRenderersEvent;
import net.minecraftforge.api.distmarker.Dist;

import net.mcreator.modscitymod.client.renderer.NpcRenderer;
import net.mcreator.modscitymod.client.renderer.Npc8Renderer;
import net.mcreator.modscitymod.client.renderer.Npc7Renderer;
import net.mcreator.modscitymod.client.renderer.Npc6Renderer;
import net.mcreator.modscitymod.client.renderer.Npc5Renderer;
import net.mcreator.modscitymod.client.renderer.Npc4Renderer;
import net.mcreator.modscitymod.client.renderer.Npc3Renderer;
import net.mcreator.modscitymod.client.renderer.Npc2Renderer;
import net.mcreator.modscitymod.client.renderer.Npc1Renderer;
import net.mcreator.modscitymod.client.renderer.KartRenderer;

@Mod.EventBusSubscriber(bus = Mod.EventBusSubscriber.Bus.MOD, value = Dist.CLIENT)
public class ModsCityModModEntityRenderers {
	@SubscribeEvent
	public static void registerEntityRenderers(EntityRenderersEvent.RegisterRenderers event) {
		event.registerEntityRenderer(ModsCityModModEntities.NPC.get(), NpcRenderer::new);
		event.registerEntityRenderer(ModsCityModModEntities.KART.get(), KartRenderer::new);
		event.registerEntityRenderer(ModsCityModModEntities.NPC_6.get(), Npc6Renderer::new);
		event.registerEntityRenderer(ModsCityModModEntities.NPC_1.get(), Npc1Renderer::new);
		event.registerEntityRenderer(ModsCityModModEntities.NPC_2.get(), Npc2Renderer::new);
		event.registerEntityRenderer(ModsCityModModEntities.NPC_3.get(), Npc3Renderer::new);
		event.registerEntityRenderer(ModsCityModModEntities.NPC_4.get(), Npc4Renderer::new);
		event.registerEntityRenderer(ModsCityModModEntities.NPC_5.get(), Npc5Renderer::new);
		event.registerEntityRenderer(ModsCityModModEntities.NPC_7.get(), Npc7Renderer::new);
		event.registerEntityRenderer(ModsCityModModEntities.NPC_8.get(), Npc8Renderer::new);
	}
}
