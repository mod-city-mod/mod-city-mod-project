
/*
 *    MCreator note: This file will be REGENERATED on each build.
 */
package net.mcreator.modscitymod.init;

import net.minecraftforge.api.distmarker.OnlyIn;
import net.minecraftforge.api.distmarker.Dist;

import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.CreativeModeTab;

public class ModsCityModModTabs {
	public static CreativeModeTab TAB_MOD_CITY;
	public static CreativeModeTab TAB_MODS_PHONES;
	public static CreativeModeTab TAB_HOLIDAYS;

	public static void load() {
		TAB_MOD_CITY = new CreativeModeTab("tabmod_city") {
			@Override
			public ItemStack makeIcon() {
				return new ItemStack(ModsCityModModBlocks.STATUE.get());
			}

			@OnlyIn(Dist.CLIENT)
			public boolean hasSearchBar() {
				return false;
			}
		};
		TAB_MODS_PHONES = new CreativeModeTab("tabmods_phones") {
			@Override
			public ItemStack makeIcon() {
				return new ItemStack(ModsCityModModItems.MODS_PHONE_7_LEMONGRASS.get());
			}

			@OnlyIn(Dist.CLIENT)
			public boolean hasSearchBar() {
				return false;
			}
		};
		TAB_HOLIDAYS = new CreativeModeTab("tabholidays") {
			@Override
			public ItemStack makeIcon() {
				return new ItemStack(ModsCityModModBlocks.CHRISTMAS_TREE.get());
			}

			@OnlyIn(Dist.CLIENT)
			public boolean hasSearchBar() {
				return false;
			}
		};
	}
}
