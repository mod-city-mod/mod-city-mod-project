
/*
 *    MCreator note: This file will be REGENERATED on each build.
 */
package net.mcreator.modscitymod.init;

import net.minecraftforge.registries.RegistryObject;
import net.minecraftforge.registries.ForgeRegistries;
import net.minecraftforge.registries.DeferredRegister;

import net.minecraft.world.level.block.entity.BlockEntityType;
import net.minecraft.world.level.block.Block;

import net.mcreator.modscitymod.block.entity.VehicleVoidBlockEntity;
import net.mcreator.modscitymod.block.entity.VehicleCheckpointBlockEntity;
import net.mcreator.modscitymod.block.entity.SwitchWithDockBlockEntity;
import net.mcreator.modscitymod.block.entity.SwitchDockBlockEntity;
import net.mcreator.modscitymod.block.entity.ServerBlockEntity;
import net.mcreator.modscitymod.block.entity.NinSwitchNoJCBlockEntity;
import net.mcreator.modscitymod.block.entity.NinSwitchBlockEntity;
import net.mcreator.modscitymod.block.entity.KartTrickBlockBlockEntity;
import net.mcreator.modscitymod.block.entity.GiftOpening5BlockEntity;
import net.mcreator.modscitymod.block.entity.GiftOpening4BlockEntity;
import net.mcreator.modscitymod.block.entity.GiftOpening3BlockEntity;
import net.mcreator.modscitymod.block.entity.GiftOpening2BlockEntity;
import net.mcreator.modscitymod.block.entity.GiftOpening1BlockEntity;
import net.mcreator.modscitymod.block.entity.GiftBlockEntity;
import net.mcreator.modscitymod.block.entity.GamingPCBlockEntity;
import net.mcreator.modscitymod.block.entity.ComputerBlockEntity;
import net.mcreator.modscitymod.block.entity.CashRegisterBlockEntity;
import net.mcreator.modscitymod.block.entity.BoxBlockEntity;
import net.mcreator.modscitymod.block.entity.AtmBlockEntity;
import net.mcreator.modscitymod.ModsCityModMod;

public class ModsCityModModBlockEntities {
	public static final DeferredRegister<BlockEntityType<?>> REGISTRY = DeferredRegister.create(ForgeRegistries.BLOCK_ENTITIES, ModsCityModMod.MODID);
	public static final RegistryObject<BlockEntityType<?>> COMPUTER = register("computer", ModsCityModModBlocks.COMPUTER, ComputerBlockEntity::new);
	public static final RegistryObject<BlockEntityType<?>> GAMING_PC = register("gaming_pc", ModsCityModModBlocks.GAMING_PC, GamingPCBlockEntity::new);
	public static final RegistryObject<BlockEntityType<?>> SERVER = register("server", ModsCityModModBlocks.SERVER, ServerBlockEntity::new);
	public static final RegistryObject<BlockEntityType<?>> CASH_REGISTER = register("cash_register", ModsCityModModBlocks.CASH_REGISTER, CashRegisterBlockEntity::new);
	public static final RegistryObject<BlockEntityType<?>> ATM = register("atm", ModsCityModModBlocks.ATM, AtmBlockEntity::new);
	public static final RegistryObject<BlockEntityType<?>> NIN_SWITCH = register("nin_switch", ModsCityModModBlocks.NIN_SWITCH, NinSwitchBlockEntity::new);
	public static final RegistryObject<BlockEntityType<?>> SWITCH_DOCK = register("switch_dock", ModsCityModModBlocks.SWITCH_DOCK, SwitchDockBlockEntity::new);
	public static final RegistryObject<BlockEntityType<?>> BOX = register("box", ModsCityModModBlocks.BOX, BoxBlockEntity::new);
	public static final RegistryObject<BlockEntityType<?>> NIN_SWITCH_NO_JC = register("nin_switch_no_jc", ModsCityModModBlocks.NIN_SWITCH_NO_JC, NinSwitchNoJCBlockEntity::new);
	public static final RegistryObject<BlockEntityType<?>> SWITCH_WITH_DOCK = register("switch_with_dock", ModsCityModModBlocks.SWITCH_WITH_DOCK, SwitchWithDockBlockEntity::new);
	public static final RegistryObject<BlockEntityType<?>> GIFT = register("gift", ModsCityModModBlocks.GIFT, GiftBlockEntity::new);
	public static final RegistryObject<BlockEntityType<?>> GIFT_OPENING_1 = register("gift_opening_1", ModsCityModModBlocks.GIFT_OPENING_1, GiftOpening1BlockEntity::new);
	public static final RegistryObject<BlockEntityType<?>> GIFT_OPENING_2 = register("gift_opening_2", ModsCityModModBlocks.GIFT_OPENING_2, GiftOpening2BlockEntity::new);
	public static final RegistryObject<BlockEntityType<?>> GIFT_OPENING_3 = register("gift_opening_3", ModsCityModModBlocks.GIFT_OPENING_3, GiftOpening3BlockEntity::new);
	public static final RegistryObject<BlockEntityType<?>> GIFT_OPENING_4 = register("gift_opening_4", ModsCityModModBlocks.GIFT_OPENING_4, GiftOpening4BlockEntity::new);
	public static final RegistryObject<BlockEntityType<?>> GIFT_OPENING_5 = register("gift_opening_5", ModsCityModModBlocks.GIFT_OPENING_5, GiftOpening5BlockEntity::new);
	public static final RegistryObject<BlockEntityType<?>> VEHICLE_VOID = register("vehicle_void", ModsCityModModBlocks.VEHICLE_VOID, VehicleVoidBlockEntity::new);
	public static final RegistryObject<BlockEntityType<?>> KART_TRICK_BLOCK = register("kart_trick_block", ModsCityModModBlocks.KART_TRICK_BLOCK, KartTrickBlockBlockEntity::new);
	public static final RegistryObject<BlockEntityType<?>> VEHICLE_CHECKPOINT = register("vehicle_checkpoint", ModsCityModModBlocks.VEHICLE_CHECKPOINT, VehicleCheckpointBlockEntity::new);

	private static RegistryObject<BlockEntityType<?>> register(String registryname, RegistryObject<Block> block, BlockEntityType.BlockEntitySupplier<?> supplier) {
		return REGISTRY.register(registryname, () -> BlockEntityType.Builder.of(supplier, block.get()).build(null));
	}
}
