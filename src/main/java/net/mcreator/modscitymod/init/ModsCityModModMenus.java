
/*
 *    MCreator note: This file will be REGENERATED on each build.
 */
package net.mcreator.modscitymod.init;

import net.minecraftforge.network.IContainerFactory;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.event.RegistryEvent;

import net.minecraft.world.inventory.MenuType;
import net.minecraft.world.inventory.AbstractContainerMenu;

import net.mcreator.modscitymod.world.inventory.WorkerBagGuiMenu;
import net.mcreator.modscitymod.world.inventory.WithdrawMenu;
import net.mcreator.modscitymod.world.inventory.WallpaperGUIMenu;
import net.mcreator.modscitymod.world.inventory.SmartPhoneGUIOFFMenu;
import net.mcreator.modscitymod.world.inventory.SmartPhoneGUIMenu;
import net.mcreator.modscitymod.world.inventory.SmartPhoneGUI2Menu;
import net.mcreator.modscitymod.world.inventory.SmartPhoneGUI1Menu;
import net.mcreator.modscitymod.world.inventory.SignCardGUIMenu;
import net.mcreator.modscitymod.world.inventory.SettingsGUIMenu;
import net.mcreator.modscitymod.world.inventory.ServerDataMenu;
import net.mcreator.modscitymod.world.inventory.MsgAppMobileMenu;
import net.mcreator.modscitymod.world.inventory.MsgAppMenu;
import net.mcreator.modscitymod.world.inventory.InterwebsGuiMenu;
import net.mcreator.modscitymod.world.inventory.GamemodesMenu;
import net.mcreator.modscitymod.world.inventory.EmployeeGUIMenu;
import net.mcreator.modscitymod.world.inventory.DepositMenu;
import net.mcreator.modscitymod.world.inventory.CustomerGUIMenu;
import net.mcreator.modscitymod.world.inventory.CookieBoxGUIMenu;
import net.mcreator.modscitymod.world.inventory.ComputerGUIMenu;
import net.mcreator.modscitymod.world.inventory.ChangeNPCSkinGUI8Menu;
import net.mcreator.modscitymod.world.inventory.ChangeNPCSkinGUI7Menu;
import net.mcreator.modscitymod.world.inventory.ChangeNPCSkinGUI6Menu;
import net.mcreator.modscitymod.world.inventory.ChangeNPCSkinGUI5Menu;
import net.mcreator.modscitymod.world.inventory.ChangeNPCSkinGUI4Menu;
import net.mcreator.modscitymod.world.inventory.ChangeNPCSkinGUI3Menu;
import net.mcreator.modscitymod.world.inventory.ChangeNPCSkinGUI2Menu;
import net.mcreator.modscitymod.world.inventory.ChangeNPCSkinGUI1Menu;
import net.mcreator.modscitymod.world.inventory.ChangeNPCSkinGUI0Menu;
import net.mcreator.modscitymod.world.inventory.CashRegisterGuiMenu;
import net.mcreator.modscitymod.world.inventory.CandyBagGUIMenu;
import net.mcreator.modscitymod.world.inventory.BoxGUIMenu;
import net.mcreator.modscitymod.world.inventory.BdscookiesSiteMenu;
import net.mcreator.modscitymod.world.inventory.AtmGUIMenu;

import java.util.List;
import java.util.ArrayList;

@Mod.EventBusSubscriber(bus = Mod.EventBusSubscriber.Bus.MOD)
public class ModsCityModModMenus {
	private static final List<MenuType<?>> REGISTRY = new ArrayList<>();
	public static final MenuType<ComputerGUIMenu> COMPUTER_GUI = register("computer_gui", (id, inv, extraData) -> new ComputerGUIMenu(id, inv, extraData));
	public static final MenuType<GamemodesMenu> GAMEMODES = register("gamemodes", (id, inv, extraData) -> new GamemodesMenu(id, inv, extraData));
	public static final MenuType<WorkerBagGuiMenu> WORKER_BAG_GUI = register("worker_bag_gui", (id, inv, extraData) -> new WorkerBagGuiMenu(id, inv, extraData));
	public static final MenuType<ServerDataMenu> SERVER_DATA = register("server_data", (id, inv, extraData) -> new ServerDataMenu(id, inv, extraData));
	public static final MenuType<MsgAppMenu> MSG_APP = register("msg_app", (id, inv, extraData) -> new MsgAppMenu(id, inv, extraData));
	public static final MenuType<InterwebsGuiMenu> INTERWEBS_GUI = register("interwebs_gui", (id, inv, extraData) -> new InterwebsGuiMenu(id, inv, extraData));
	public static final MenuType<BdscookiesSiteMenu> BDSCOOKIES_SITE = register("bdscookies_site", (id, inv, extraData) -> new BdscookiesSiteMenu(id, inv, extraData));
	public static final MenuType<EmployeeGUIMenu> EMPLOYEE_GUI = register("employee_gui", (id, inv, extraData) -> new EmployeeGUIMenu(id, inv, extraData));
	public static final MenuType<CashRegisterGuiMenu> CASH_REGISTER_GUI = register("cash_register_gui", (id, inv, extraData) -> new CashRegisterGuiMenu(id, inv, extraData));
	public static final MenuType<SignCardGUIMenu> SIGN_CARD_GUI = register("sign_card_gui", (id, inv, extraData) -> new SignCardGUIMenu(id, inv, extraData));
	public static final MenuType<CustomerGUIMenu> CUSTOMER_GUI = register("customer_gui", (id, inv, extraData) -> new CustomerGUIMenu(id, inv, extraData));
	public static final MenuType<AtmGUIMenu> ATM_GUI = register("atm_gui", (id, inv, extraData) -> new AtmGUIMenu(id, inv, extraData));
	public static final MenuType<DepositMenu> DEPOSIT = register("deposit", (id, inv, extraData) -> new DepositMenu(id, inv, extraData));
	public static final MenuType<WithdrawMenu> WITHDRAW = register("withdraw", (id, inv, extraData) -> new WithdrawMenu(id, inv, extraData));
	public static final MenuType<BoxGUIMenu> BOX_GUI = register("box_gui", (id, inv, extraData) -> new BoxGUIMenu(id, inv, extraData));
	public static final MenuType<CookieBoxGUIMenu> COOKIE_BOX_GUI = register("cookie_box_gui", (id, inv, extraData) -> new CookieBoxGUIMenu(id, inv, extraData));
	public static final MenuType<SettingsGUIMenu> SETTINGS_GUI = register("settings_gui", (id, inv, extraData) -> new SettingsGUIMenu(id, inv, extraData));
	public static final MenuType<SmartPhoneGUIOFFMenu> SMART_PHONE_GUIOFF = register("smart_phone_guioff", (id, inv, extraData) -> new SmartPhoneGUIOFFMenu(id, inv, extraData));
	public static final MenuType<SmartPhoneGUIMenu> SMART_PHONE_GUI = register("smart_phone_gui", (id, inv, extraData) -> new SmartPhoneGUIMenu(id, inv, extraData));
	public static final MenuType<SmartPhoneGUI1Menu> SMART_PHONE_GUI_1 = register("smart_phone_gui_1", (id, inv, extraData) -> new SmartPhoneGUI1Menu(id, inv, extraData));
	public static final MenuType<WallpaperGUIMenu> WALLPAPER_GUI = register("wallpaper_gui", (id, inv, extraData) -> new WallpaperGUIMenu(id, inv, extraData));
	public static final MenuType<CandyBagGUIMenu> CANDY_BAG_GUI = register("candy_bag_gui", (id, inv, extraData) -> new CandyBagGUIMenu(id, inv, extraData));
	public static final MenuType<SmartPhoneGUI2Menu> SMART_PHONE_GUI_2 = register("smart_phone_gui_2", (id, inv, extraData) -> new SmartPhoneGUI2Menu(id, inv, extraData));
	public static final MenuType<ChangeNPCSkinGUI0Menu> CHANGE_NPC_SKIN_GUI_0 = register("change_npc_skin_gui_0", (id, inv, extraData) -> new ChangeNPCSkinGUI0Menu(id, inv, extraData));
	public static final MenuType<ChangeNPCSkinGUI1Menu> CHANGE_NPC_SKIN_GUI_1 = register("change_npc_skin_gui_1", (id, inv, extraData) -> new ChangeNPCSkinGUI1Menu(id, inv, extraData));
	public static final MenuType<ChangeNPCSkinGUI2Menu> CHANGE_NPC_SKIN_GUI_2 = register("change_npc_skin_gui_2", (id, inv, extraData) -> new ChangeNPCSkinGUI2Menu(id, inv, extraData));
	public static final MenuType<ChangeNPCSkinGUI3Menu> CHANGE_NPC_SKIN_GUI_3 = register("change_npc_skin_gui_3", (id, inv, extraData) -> new ChangeNPCSkinGUI3Menu(id, inv, extraData));
	public static final MenuType<ChangeNPCSkinGUI4Menu> CHANGE_NPC_SKIN_GUI_4 = register("change_npc_skin_gui_4", (id, inv, extraData) -> new ChangeNPCSkinGUI4Menu(id, inv, extraData));
	public static final MenuType<ChangeNPCSkinGUI5Menu> CHANGE_NPC_SKIN_GUI_5 = register("change_npc_skin_gui_5", (id, inv, extraData) -> new ChangeNPCSkinGUI5Menu(id, inv, extraData));
	public static final MenuType<ChangeNPCSkinGUI6Menu> CHANGE_NPC_SKIN_GUI_6 = register("change_npc_skin_gui_6", (id, inv, extraData) -> new ChangeNPCSkinGUI6Menu(id, inv, extraData));
	public static final MenuType<ChangeNPCSkinGUI7Menu> CHANGE_NPC_SKIN_GUI_7 = register("change_npc_skin_gui_7", (id, inv, extraData) -> new ChangeNPCSkinGUI7Menu(id, inv, extraData));
	public static final MenuType<ChangeNPCSkinGUI8Menu> CHANGE_NPC_SKIN_GUI_8 = register("change_npc_skin_gui_8", (id, inv, extraData) -> new ChangeNPCSkinGUI8Menu(id, inv, extraData));
	public static final MenuType<MsgAppMobileMenu> MSG_APP_MOBILE = register("msg_app_mobile", (id, inv, extraData) -> new MsgAppMobileMenu(id, inv, extraData));

	private static <T extends AbstractContainerMenu> MenuType<T> register(String registryname, IContainerFactory<T> containerFactory) {
		MenuType<T> menuType = new MenuType<T>(containerFactory);
		menuType.setRegistryName(registryname);
		REGISTRY.add(menuType);
		return menuType;
	}

	@SubscribeEvent
	public static void registerContainers(RegistryEvent.Register<MenuType<?>> event) {
		event.getRegistry().registerAll(REGISTRY.toArray(new MenuType[0]));
	}
}
