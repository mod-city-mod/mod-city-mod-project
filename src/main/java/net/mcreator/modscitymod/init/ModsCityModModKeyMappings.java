
/*
 *    MCreator note: This file will be REGENERATED on each build.
 */
package net.mcreator.modscitymod.init;

import org.lwjgl.glfw.GLFW;

import net.minecraftforge.fml.event.lifecycle.FMLClientSetupEvent;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.client.event.InputEvent;
import net.minecraftforge.client.ClientRegistry;
import net.minecraftforge.api.distmarker.Dist;

import net.minecraft.client.Minecraft;
import net.minecraft.client.KeyMapping;

import net.mcreator.modscitymod.network.KartTrickMessage;
import net.mcreator.modscitymod.ModsCityModMod;

@Mod.EventBusSubscriber(bus = Mod.EventBusSubscriber.Bus.MOD, value = {Dist.CLIENT})
public class ModsCityModModKeyMappings {
	public static final KeyMapping KART_TRICK = new KeyMapping("key.mods_city_mod.kart_trick", GLFW.GLFW_KEY_C, "key.categories.movement");

	@SubscribeEvent
	public static void registerKeyBindings(FMLClientSetupEvent event) {
		ClientRegistry.registerKeyBinding(KART_TRICK);
	}

	@Mod.EventBusSubscriber({Dist.CLIENT})
	public static class KeyEventListener {
		@SubscribeEvent
		public static void onKeyInput(InputEvent.KeyInputEvent event) {
			if (Minecraft.getInstance().screen == null) {
				if (event.getKey() == KART_TRICK.getKey().getValue()) {
					if (event.getAction() == GLFW.GLFW_PRESS) {
						ModsCityModMod.PACKET_HANDLER.sendToServer(new KartTrickMessage(0, 0));
						KartTrickMessage.pressAction(Minecraft.getInstance().player, 0, 0);
					}
				}
			}
		}
	}
}
