
/*
 *    MCreator note: This file will be REGENERATED on each build.
 */
package net.mcreator.modscitymod.init;

import net.minecraftforge.fml.event.lifecycle.FMLClientSetupEvent;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.api.distmarker.Dist;

import net.minecraft.client.gui.screens.MenuScreens;

import net.mcreator.modscitymod.client.gui.WorkerBagGuiScreen;
import net.mcreator.modscitymod.client.gui.WithdrawScreen;
import net.mcreator.modscitymod.client.gui.WallpaperGUIScreen;
import net.mcreator.modscitymod.client.gui.SmartPhoneGUIScreen;
import net.mcreator.modscitymod.client.gui.SmartPhoneGUIOFFScreen;
import net.mcreator.modscitymod.client.gui.SmartPhoneGUI2Screen;
import net.mcreator.modscitymod.client.gui.SmartPhoneGUI1Screen;
import net.mcreator.modscitymod.client.gui.SignCardGUIScreen;
import net.mcreator.modscitymod.client.gui.SettingsGUIScreen;
import net.mcreator.modscitymod.client.gui.ServerDataScreen;
import net.mcreator.modscitymod.client.gui.MsgAppScreen;
import net.mcreator.modscitymod.client.gui.MsgAppMobileScreen;
import net.mcreator.modscitymod.client.gui.InterwebsGuiScreen;
import net.mcreator.modscitymod.client.gui.GamemodesScreen;
import net.mcreator.modscitymod.client.gui.EmployeeGUIScreen;
import net.mcreator.modscitymod.client.gui.DepositScreen;
import net.mcreator.modscitymod.client.gui.CustomerGUIScreen;
import net.mcreator.modscitymod.client.gui.CookieBoxGUIScreen;
import net.mcreator.modscitymod.client.gui.ComputerGUIScreen;
import net.mcreator.modscitymod.client.gui.ChangeNPCSkinGUI8Screen;
import net.mcreator.modscitymod.client.gui.ChangeNPCSkinGUI7Screen;
import net.mcreator.modscitymod.client.gui.ChangeNPCSkinGUI6Screen;
import net.mcreator.modscitymod.client.gui.ChangeNPCSkinGUI5Screen;
import net.mcreator.modscitymod.client.gui.ChangeNPCSkinGUI4Screen;
import net.mcreator.modscitymod.client.gui.ChangeNPCSkinGUI3Screen;
import net.mcreator.modscitymod.client.gui.ChangeNPCSkinGUI2Screen;
import net.mcreator.modscitymod.client.gui.ChangeNPCSkinGUI1Screen;
import net.mcreator.modscitymod.client.gui.ChangeNPCSkinGUI0Screen;
import net.mcreator.modscitymod.client.gui.CashRegisterGuiScreen;
import net.mcreator.modscitymod.client.gui.CandyBagGUIScreen;
import net.mcreator.modscitymod.client.gui.BoxGUIScreen;
import net.mcreator.modscitymod.client.gui.BdscookiesSiteScreen;
import net.mcreator.modscitymod.client.gui.AtmGUIScreen;

@Mod.EventBusSubscriber(bus = Mod.EventBusSubscriber.Bus.MOD, value = Dist.CLIENT)
public class ModsCityModModScreens {
	@SubscribeEvent
	public static void clientLoad(FMLClientSetupEvent event) {
		event.enqueueWork(() -> {
			MenuScreens.register(ModsCityModModMenus.COMPUTER_GUI, ComputerGUIScreen::new);
			MenuScreens.register(ModsCityModModMenus.GAMEMODES, GamemodesScreen::new);
			MenuScreens.register(ModsCityModModMenus.WORKER_BAG_GUI, WorkerBagGuiScreen::new);
			MenuScreens.register(ModsCityModModMenus.SERVER_DATA, ServerDataScreen::new);
			MenuScreens.register(ModsCityModModMenus.MSG_APP, MsgAppScreen::new);
			MenuScreens.register(ModsCityModModMenus.INTERWEBS_GUI, InterwebsGuiScreen::new);
			MenuScreens.register(ModsCityModModMenus.BDSCOOKIES_SITE, BdscookiesSiteScreen::new);
			MenuScreens.register(ModsCityModModMenus.EMPLOYEE_GUI, EmployeeGUIScreen::new);
			MenuScreens.register(ModsCityModModMenus.CASH_REGISTER_GUI, CashRegisterGuiScreen::new);
			MenuScreens.register(ModsCityModModMenus.SIGN_CARD_GUI, SignCardGUIScreen::new);
			MenuScreens.register(ModsCityModModMenus.CUSTOMER_GUI, CustomerGUIScreen::new);
			MenuScreens.register(ModsCityModModMenus.ATM_GUI, AtmGUIScreen::new);
			MenuScreens.register(ModsCityModModMenus.DEPOSIT, DepositScreen::new);
			MenuScreens.register(ModsCityModModMenus.WITHDRAW, WithdrawScreen::new);
			MenuScreens.register(ModsCityModModMenus.BOX_GUI, BoxGUIScreen::new);
			MenuScreens.register(ModsCityModModMenus.COOKIE_BOX_GUI, CookieBoxGUIScreen::new);
			MenuScreens.register(ModsCityModModMenus.SETTINGS_GUI, SettingsGUIScreen::new);
			MenuScreens.register(ModsCityModModMenus.SMART_PHONE_GUIOFF, SmartPhoneGUIOFFScreen::new);
			MenuScreens.register(ModsCityModModMenus.SMART_PHONE_GUI, SmartPhoneGUIScreen::new);
			MenuScreens.register(ModsCityModModMenus.SMART_PHONE_GUI_1, SmartPhoneGUI1Screen::new);
			MenuScreens.register(ModsCityModModMenus.WALLPAPER_GUI, WallpaperGUIScreen::new);
			MenuScreens.register(ModsCityModModMenus.CANDY_BAG_GUI, CandyBagGUIScreen::new);
			MenuScreens.register(ModsCityModModMenus.SMART_PHONE_GUI_2, SmartPhoneGUI2Screen::new);
			MenuScreens.register(ModsCityModModMenus.CHANGE_NPC_SKIN_GUI_0, ChangeNPCSkinGUI0Screen::new);
			MenuScreens.register(ModsCityModModMenus.CHANGE_NPC_SKIN_GUI_1, ChangeNPCSkinGUI1Screen::new);
			MenuScreens.register(ModsCityModModMenus.CHANGE_NPC_SKIN_GUI_2, ChangeNPCSkinGUI2Screen::new);
			MenuScreens.register(ModsCityModModMenus.CHANGE_NPC_SKIN_GUI_3, ChangeNPCSkinGUI3Screen::new);
			MenuScreens.register(ModsCityModModMenus.CHANGE_NPC_SKIN_GUI_4, ChangeNPCSkinGUI4Screen::new);
			MenuScreens.register(ModsCityModModMenus.CHANGE_NPC_SKIN_GUI_5, ChangeNPCSkinGUI5Screen::new);
			MenuScreens.register(ModsCityModModMenus.CHANGE_NPC_SKIN_GUI_6, ChangeNPCSkinGUI6Screen::new);
			MenuScreens.register(ModsCityModModMenus.CHANGE_NPC_SKIN_GUI_7, ChangeNPCSkinGUI7Screen::new);
			MenuScreens.register(ModsCityModModMenus.CHANGE_NPC_SKIN_GUI_8, ChangeNPCSkinGUI8Screen::new);
			MenuScreens.register(ModsCityModModMenus.MSG_APP_MOBILE, MsgAppMobileScreen::new);
		});
	}
}
