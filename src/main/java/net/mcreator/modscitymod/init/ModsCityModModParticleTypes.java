
/*
 *    MCreator note: This file will be REGENERATED on each build.
 */
package net.mcreator.modscitymod.init;

import net.minecraftforge.registries.RegistryObject;
import net.minecraftforge.registries.ForgeRegistries;
import net.minecraftforge.registries.DeferredRegister;

import net.minecraft.core.particles.SimpleParticleType;
import net.minecraft.core.particles.ParticleType;

import net.mcreator.modscitymod.ModsCityModMod;

public class ModsCityModModParticleTypes {
	public static final DeferredRegister<ParticleType<?>> REGISTRY = DeferredRegister.create(ForgeRegistries.PARTICLE_TYPES, ModsCityModMod.MODID);
	public static final RegistryObject<ParticleType<?>> VEHICLE_VOID_DISPLAY = REGISTRY.register("vehicle_void_display", () -> new SimpleParticleType(true));
	public static final RegistryObject<ParticleType<?>> OFFROAD_DISPLAY = REGISTRY.register("offroad_display", () -> new SimpleParticleType(true));
	public static final RegistryObject<ParticleType<?>> KART_TRICK_DISPLAY = REGISTRY.register("kart_trick_display", () -> new SimpleParticleType(true));
	public static final RegistryObject<ParticleType<?>> VEHICLE_CHECKPOINT_DISPLAY = REGISTRY.register("vehicle_checkpoint_display", () -> new SimpleParticleType(true));
}
