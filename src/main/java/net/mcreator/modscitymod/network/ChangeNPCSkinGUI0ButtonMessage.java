
package net.mcreator.modscitymod.network;

import net.minecraftforge.network.NetworkEvent;
import net.minecraftforge.fml.event.lifecycle.FMLCommonSetupEvent;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.eventbus.api.SubscribeEvent;

import net.minecraft.world.level.Level;
import net.minecraft.world.entity.player.Player;
import net.minecraft.network.FriendlyByteBuf;
import net.minecraft.core.BlockPos;

import net.mcreator.modscitymod.world.inventory.ChangeNPCSkinGUI0Menu;
import net.mcreator.modscitymod.procedures.NPCSet0Procedure;
import net.mcreator.modscitymod.procedures.ChangeNPCSkinGUIProc1Procedure;
import net.mcreator.modscitymod.ModsCityModMod;

import java.util.function.Supplier;
import java.util.HashMap;

@Mod.EventBusSubscriber(bus = Mod.EventBusSubscriber.Bus.MOD)
public class ChangeNPCSkinGUI0ButtonMessage {
	private final int buttonID, x, y, z;

	public ChangeNPCSkinGUI0ButtonMessage(FriendlyByteBuf buffer) {
		this.buttonID = buffer.readInt();
		this.x = buffer.readInt();
		this.y = buffer.readInt();
		this.z = buffer.readInt();
	}

	public ChangeNPCSkinGUI0ButtonMessage(int buttonID, int x, int y, int z) {
		this.buttonID = buttonID;
		this.x = x;
		this.y = y;
		this.z = z;
	}

	public static void buffer(ChangeNPCSkinGUI0ButtonMessage message, FriendlyByteBuf buffer) {
		buffer.writeInt(message.buttonID);
		buffer.writeInt(message.x);
		buffer.writeInt(message.y);
		buffer.writeInt(message.z);
	}

	public static void handler(ChangeNPCSkinGUI0ButtonMessage message, Supplier<NetworkEvent.Context> contextSupplier) {
		NetworkEvent.Context context = contextSupplier.get();
		context.enqueueWork(() -> {
			Player entity = context.getSender();
			int buttonID = message.buttonID;
			int x = message.x;
			int y = message.y;
			int z = message.z;
			handleButtonAction(entity, buttonID, x, y, z);
		});
		context.setPacketHandled(true);
	}

	public static void handleButtonAction(Player entity, int buttonID, int x, int y, int z) {
		Level world = entity.level;
		HashMap guistate = ChangeNPCSkinGUI0Menu.guistate;
		// security measure to prevent arbitrary chunk generation
		if (!world.hasChunkAt(new BlockPos(x, y, z)))
			return;
		if (buttonID == 0) {

			ChangeNPCSkinGUIProc1Procedure.execute(world, x, y, z, entity);
		}
		if (buttonID == 1) {

			NPCSet0Procedure.execute(world, x, y, z);
		}
	}

	@SubscribeEvent
	public static void registerMessage(FMLCommonSetupEvent event) {
		ModsCityModMod.addNetworkMessage(ChangeNPCSkinGUI0ButtonMessage.class, ChangeNPCSkinGUI0ButtonMessage::buffer, ChangeNPCSkinGUI0ButtonMessage::new, ChangeNPCSkinGUI0ButtonMessage::handler);
	}
}
