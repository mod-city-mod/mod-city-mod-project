
package net.mcreator.modscitymod.network;

import net.minecraftforge.network.NetworkEvent;
import net.minecraftforge.fml.event.lifecycle.FMLCommonSetupEvent;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.eventbus.api.SubscribeEvent;

import net.minecraft.world.level.Level;
import net.minecraft.world.entity.player.Player;
import net.minecraft.network.FriendlyByteBuf;
import net.minecraft.core.BlockPos;

import net.mcreator.modscitymod.world.inventory.GamemodesMenu;
import net.mcreator.modscitymod.procedures.SurvivalmodeproceProcedure;
import net.mcreator.modscitymod.procedures.SpectatormodeproceProcedure;
import net.mcreator.modscitymod.procedures.CreativemodeproceProcedure;
import net.mcreator.modscitymod.procedures.CloseGUIProcedure;
import net.mcreator.modscitymod.procedures.AdventuremodeproceProcedure;
import net.mcreator.modscitymod.ModsCityModMod;

import java.util.function.Supplier;
import java.util.HashMap;

@Mod.EventBusSubscriber(bus = Mod.EventBusSubscriber.Bus.MOD)
public class GamemodesButtonMessage {
	private final int buttonID, x, y, z;

	public GamemodesButtonMessage(FriendlyByteBuf buffer) {
		this.buttonID = buffer.readInt();
		this.x = buffer.readInt();
		this.y = buffer.readInt();
		this.z = buffer.readInt();
	}

	public GamemodesButtonMessage(int buttonID, int x, int y, int z) {
		this.buttonID = buttonID;
		this.x = x;
		this.y = y;
		this.z = z;
	}

	public static void buffer(GamemodesButtonMessage message, FriendlyByteBuf buffer) {
		buffer.writeInt(message.buttonID);
		buffer.writeInt(message.x);
		buffer.writeInt(message.y);
		buffer.writeInt(message.z);
	}

	public static void handler(GamemodesButtonMessage message, Supplier<NetworkEvent.Context> contextSupplier) {
		NetworkEvent.Context context = contextSupplier.get();
		context.enqueueWork(() -> {
			Player entity = context.getSender();
			int buttonID = message.buttonID;
			int x = message.x;
			int y = message.y;
			int z = message.z;
			handleButtonAction(entity, buttonID, x, y, z);
		});
		context.setPacketHandled(true);
	}

	public static void handleButtonAction(Player entity, int buttonID, int x, int y, int z) {
		Level world = entity.level;
		HashMap guistate = GamemodesMenu.guistate;
		// security measure to prevent arbitrary chunk generation
		if (!world.hasChunkAt(new BlockPos(x, y, z)))
			return;
		if (buttonID == 0) {

			SurvivalmodeproceProcedure.execute(entity);
		}
		if (buttonID == 1) {

			CreativemodeproceProcedure.execute(entity);
		}
		if (buttonID == 2) {

			SpectatormodeproceProcedure.execute(entity);
		}
		if (buttonID == 3) {

			AdventuremodeproceProcedure.execute(entity);
		}
		if (buttonID == 4) {

			CloseGUIProcedure.execute(entity);
		}
	}

	@SubscribeEvent
	public static void registerMessage(FMLCommonSetupEvent event) {
		ModsCityModMod.addNetworkMessage(GamemodesButtonMessage.class, GamemodesButtonMessage::buffer, GamemodesButtonMessage::new, GamemodesButtonMessage::handler);
	}
}
