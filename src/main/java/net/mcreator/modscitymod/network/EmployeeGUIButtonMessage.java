
package net.mcreator.modscitymod.network;

import net.minecraftforge.network.NetworkEvent;
import net.minecraftforge.fml.event.lifecycle.FMLCommonSetupEvent;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.eventbus.api.SubscribeEvent;

import net.minecraft.world.level.Level;
import net.minecraft.world.entity.player.Player;
import net.minecraft.network.FriendlyByteBuf;
import net.minecraft.core.BlockPos;

import net.mcreator.modscitymod.world.inventory.EmployeeGUIMenu;
import net.mcreator.modscitymod.procedures.SubtractionProcProcedure;
import net.mcreator.modscitymod.procedures.RequestPaymentProcProcedure;
import net.mcreator.modscitymod.procedures.MultiplicationProcProcedure;
import net.mcreator.modscitymod.procedures.DivisionProcProcedure;
import net.mcreator.modscitymod.procedures.AdditionProcProcedure;
import net.mcreator.modscitymod.ModsCityModMod;

import java.util.function.Supplier;
import java.util.HashMap;

@Mod.EventBusSubscriber(bus = Mod.EventBusSubscriber.Bus.MOD)
public class EmployeeGUIButtonMessage {
	private final int buttonID, x, y, z;

	public EmployeeGUIButtonMessage(FriendlyByteBuf buffer) {
		this.buttonID = buffer.readInt();
		this.x = buffer.readInt();
		this.y = buffer.readInt();
		this.z = buffer.readInt();
	}

	public EmployeeGUIButtonMessage(int buttonID, int x, int y, int z) {
		this.buttonID = buttonID;
		this.x = x;
		this.y = y;
		this.z = z;
	}

	public static void buffer(EmployeeGUIButtonMessage message, FriendlyByteBuf buffer) {
		buffer.writeInt(message.buttonID);
		buffer.writeInt(message.x);
		buffer.writeInt(message.y);
		buffer.writeInt(message.z);
	}

	public static void handler(EmployeeGUIButtonMessage message, Supplier<NetworkEvent.Context> contextSupplier) {
		NetworkEvent.Context context = contextSupplier.get();
		context.enqueueWork(() -> {
			Player entity = context.getSender();
			int buttonID = message.buttonID;
			int x = message.x;
			int y = message.y;
			int z = message.z;
			handleButtonAction(entity, buttonID, x, y, z);
		});
		context.setPacketHandled(true);
	}

	public static void handleButtonAction(Player entity, int buttonID, int x, int y, int z) {
		Level world = entity.level;
		HashMap guistate = EmployeeGUIMenu.guistate;
		// security measure to prevent arbitrary chunk generation
		if (!world.hasChunkAt(new BlockPos(x, y, z)))
			return;
		if (buttonID == 0) {

			AdditionProcProcedure.execute(guistate);
		}
		if (buttonID == 1) {

			SubtractionProcProcedure.execute(guistate);
		}
		if (buttonID == 2) {

			MultiplicationProcProcedure.execute(guistate);
		}
		if (buttonID == 3) {

			DivisionProcProcedure.execute(guistate);
		}
		if (buttonID == 4) {

			RequestPaymentProcProcedure.execute(world, entity, guistate);
		}
	}

	@SubscribeEvent
	public static void registerMessage(FMLCommonSetupEvent event) {
		ModsCityModMod.addNetworkMessage(EmployeeGUIButtonMessage.class, EmployeeGUIButtonMessage::buffer, EmployeeGUIButtonMessage::new, EmployeeGUIButtonMessage::handler);
	}
}
