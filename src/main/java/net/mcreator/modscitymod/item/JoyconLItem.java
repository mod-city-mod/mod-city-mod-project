
package net.mcreator.modscitymod.item;

import net.minecraft.world.item.Rarity;
import net.minecraft.world.item.Item;

public class JoyconLItem extends Item {
	public JoyconLItem() {
		super(new Item.Properties().tab(null).stacksTo(1).rarity(Rarity.COMMON));
	}
}
