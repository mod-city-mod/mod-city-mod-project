
package net.mcreator.modscitymod.item;

import net.minecraft.world.item.Rarity;
import net.minecraft.world.item.Items;
import net.minecraft.world.item.Item;
import net.minecraft.world.item.BucketItem;

import net.mcreator.modscitymod.init.ModsCityModModTabs;
import net.mcreator.modscitymod.init.ModsCityModModFluids;

public class FuelItem extends BucketItem {
	public FuelItem() {
		super(ModsCityModModFluids.FUEL, new Item.Properties().craftRemainder(Items.BUCKET).stacksTo(1).rarity(Rarity.COMMON).tab(ModsCityModModTabs.TAB_MOD_CITY));
	}
}
