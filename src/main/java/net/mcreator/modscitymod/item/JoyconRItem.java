
package net.mcreator.modscitymod.item;

import net.minecraft.world.item.Rarity;
import net.minecraft.world.item.Item;

public class JoyconRItem extends Item {
	public JoyconRItem() {
		super(new Item.Properties().tab(null).stacksTo(1).rarity(Rarity.COMMON));
	}
}
