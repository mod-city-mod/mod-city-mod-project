
package net.mcreator.modscitymod.item;

import net.minecraft.world.item.Rarity;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.Item;
import net.minecraft.world.entity.LivingEntity;

import net.mcreator.modscitymod.procedures.BalloonEntitySwingsItemProcedure;
import net.mcreator.modscitymod.init.ModsCityModModTabs;

public class BalloonItem extends Item {
	public BalloonItem() {
		super(new Item.Properties().tab(ModsCityModModTabs.TAB_HOLIDAYS).stacksTo(64).rarity(Rarity.COMMON));
	}

	@Override
	public boolean hurtEnemy(ItemStack itemstack, LivingEntity entity, LivingEntity sourceentity) {
		boolean retval = super.hurtEnemy(itemstack, entity, sourceentity);
		BalloonEntitySwingsItemProcedure.execute(entity.level, entity.getX(), entity.getY(), entity.getZ(), sourceentity);
		return retval;
	}
}
