
package net.mcreator.modscitymod.item;

import net.minecraft.world.item.Rarity;
import net.minecraft.world.item.Item;
import net.minecraft.world.food.FoodProperties;

import net.mcreator.modscitymod.init.ModsCityModModTabs;

public class CandyItem extends Item {
	public CandyItem() {
		super(new Item.Properties().tab(ModsCityModModTabs.TAB_HOLIDAYS).stacksTo(64).rarity(Rarity.COMMON).food((new FoodProperties.Builder()).nutrition(4).saturationMod(0.3f).alwaysEat()

				.build()));
	}
}
